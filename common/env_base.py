import gym

class BaseEnv(object):
    def __init__(self, env_name=None):
        if env_name == None:
            self.env = None
        else:
            self.env = gym.make(env_name)

    def get_env(self):
        return self.env
