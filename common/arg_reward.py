class AugmentRewardBase(object):
    def __init__(self, rew_scale):
        self.func = None
        self.rew_scale = rew_scale

    def get_reward(self, **kwargs):
        return self.func(**kwargs)

class AugmentReward(AugmentRewardBase):
    def __init__(self, selector, rew_scale):
        super(AugmentReward, self).__init__(rew_scale)
        self.func = getattr(self,'ar_{}'.format(selector))

    def ar_0(self, **kwargs):
        return  self.rew_scale*kwargs.get('rew')

    def ar_1(self, **kwargs):
        if kwargs.get('done') :
            if kwargs.get('rew') == 1:
                return self.rew_scale*1
            else:
                return self.rew_scale*(-1)
        else:
            return kwargs.get('rew')
   
    def ar_2(self, *args, **kwargs):
        if kwargs.get('done') :
            if kwargs.get('rew') == 1:
                return self.rew_scale*kwargs.get('rew')
            else:
                return -1*self.rew_scale*kwargs.get('rew')
        else:
            if kwargs.get('obs') == kwargs.get('obs_'):
                return -2
            else:
               return 2

    def ar_3(self, *args, **kwargs):
        if kwargs.get('rew_length')>30:
            return 0
        return self.rew_scale*kwargs.get('rew')

    def ar_4(self, *args, **kwargs):
        if kwargs.get('rew_length')>30:
            return -1*self.rew_scale
        return self.rew_scale*kwargs.get('rew')

    def ar_5(self, **kwargs):
        if kwargs.get('rew_length')>30:
            return -1*self.rew_scale
        if kwargs.get('done') :
            if kwargs.get('rew') == 1:
                return self.rew_scale*1
            else:
                return self.rew_scale*(-1)
        else:
            return kwargs.get('rew')

    def ar_6(self, **kwargs):
        if kwargs.get('rew_length')>30:
            return -1*kwargs.get('rew_length')
        if kwargs.get('done') :
            if kwargs.get('rew') == 1:
                return self.rew_scale*1
            else:
                return self.rew_scale*(-1)
        else:
            return 1
    def ar_car(self, **kwargs):
#        print(kwargs.get('done'))
        if kwargs.get('done'):
            return self.rew_scale*(-1)
        else:
            return self.rew_scale*0.1
