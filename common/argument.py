import argparse

class ArgumentBase(object):
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.add_common()
        #self.add_specific()
       
    def add_common(self):
        self.parser.add_argument("-t", "--type",type=str, help="train or play", default='play')
        self.parser.add_argument("-c", "--cont", type=str, help="continue exist table", default='t')
        self.parser.add_argument("-e", "--greedy_denom", type=int, help="greedy_denom", default=10000)
        self.parser.add_argument("-l", "--learn_rate", type=float, help="learning rate", default=0.0001)
        self.parser.add_argument("-g", "--need_greedy", type=bool, help="need greedy", default=False)
        self.parser.add_argument("-i", "--play_load", type=bool, help="player need load", default=True)
        self.parser.add_argument("--play_render", type=bool, help="player rebder", default=False)
        self.parser.add_argument("--play_round", type=int, help="player round", default=1000)
        self.parser.add_argument("--play_clear", type=bool, help="player clear", default=False)
        self.parser.add_argument("--play_sleep", type=int, help="player sleep", default=0)
        self.parser.add_argument("--gamma", type=float, help="gamma", default=0.99)
        self.parser.add_argument("--train_epoch", type=int, help="train epoch", default=10000)
        self.parser.add_argument("--reward_type", type=str, help="reward type", default='0')
        self.parser.add_argument("--det_action", type=bool, help="deterministic action", default=True)
        self.parser.add_argument("--activation", type=str, help="activation function", default='relu')
        self.parser.add_argument("--rew_scale", type=int, help="rew_scale", default=1)
        self.parser.add_argument("--data_len", type=int, help="data length", default=1000)
        self.parser.add_argument("--batch_size", type=int, help="batch size", default=50)
        self.parser.add_argument("--optimizer", type=str, help="optimizer", default='ada')
        self.parser.add_argument("--maxtrystep", type=int, help="maxtrystep", default=5000)

    def get_args(self):
        return self.parser.parse_args()
   
    def add_specific(self, *args, **kwargs):
        print(args)
        print(kwargs)
        self.parser.add_argument(*args, **kwargs)
            
