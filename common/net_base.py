import tensorflow as tf
import numpy as np

Optimizer = {
    'ada': tf.train.AdamOptimizer,
    'rms': tf.train.RMSPropOptimizer
}

class NetBase(object):
    def __init__(self, env, namespace, learnrate, activation, optimizer):
        self.env = env
        self.namespace = namespace 
        self.q = None
        self.loss = None
        self.optimize = None
        self.loop=0
        self.learnrate = learnrate
        self.activation = activation
        self.optimizer = Optimizer.get(optimizer, tf.train.AdamOptimizer)

    def init(self):
        self.set_placeholder()
        self.net()
        self.set_optimize()

    def load(self):
        print('all trainable:', tf.trainable_variables())
        print('all dense:', [ t.name for t in  tf.trainable_variables() if 'dense' in t.name])
        print('all dense:', tf.get_default_graph().get_collection(name='dens2'))
        print('all loss:', [ t.name for t in  tf.get_default_graph().get_operations() if 'loss' in t.name])
        print('all optimize:', [ t.name for t in  tf.get_default_graph().get_operations() if 'optimize' in t.name])
        print('all diffq:', [ t.name for t in  tf.get_default_graph().get_operations() if 'diff' in t.name])
        print('all targ:', [ t.name for t in  tf.get_default_graph().get_operations() if 'targ' in t.name])
        print('all out_prob:', [ t.name for t in  tf.get_default_graph().get_operations() if 'out_prob' in t.name])
      
    def set_placeholder(self):
        raise NotImplementedError("setPlaceHolder is not implemented")

    def net(self):
        with tf.variable_scope(self.namespace):
            self.n_out = tf.layers.dense(self.obses_pro, 
                                units=36, 
                                activation=getattr(tf.nn, self.activation, None),
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                name='dense1')     
            ''' 
            self.n_out = tf.layers.dense(self.obses_pro, 
                                units=36, 
                                activation=getattr(tf.nn, self.activation, None),
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                name='dense3')       
            '''
            self.n_out = tf.layers.dense(self.n_out, 
                                activation=getattr(tf.nn, self.activation, None),
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                units=self.env.action_space.n, 
                                name='dense2')
        with tf.variable_scope('{}/'.format(self.namespace)):
            self.n_out = tf.identity(self.n_out, name='n_out')
            tf.summary.histogram('n_out', self.n_out)
            self.out_prob = tf.nn.softmax(self.n_out, name='out_prob')
            tf.summary.histogram('out_prob', self.out_prob)
        
    def set_optimize(self):
        raise NotImplementedError("optimize is not implemented")

    def plus_loop(self):
        self.loop+=1

    def get_trainable(self, scope=None):
        if scope == None:
            scope = self.namespace
        vars = tf.get_collection(key=tf.GraphKeys.GLOBAL_VARIABLES, scope=scope)
        return vars

         
class CNNNetBase(NetBase):
    def __init__(self, env, namespace, learnrate, activation, optimizer):
        NetBase.__init__(self, env, namespace, learnrate, activation, optimizer)
        
    def net(self):
        padding='SAME'
        with tf.variable_scope(self.namespace):
#            self.obses_pro = tf.Print(self.obses_pro,['<<<<<<',tf.reduce_sum(self.obses_pro[0]-self.obses_pro[-1])], summarize=1000) 
            self.n_out = tf.layers.conv2d(self.obses_pro, 
                                      filters=32, 
                                      kernel_size=[8, 8], 
                                      name='{}'.format(1), 
                                      padding=padding,
                                      activation=getattr(tf.nn, 'relu', None),
                                      kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
                                      bias_initializer=tf.constant_initializer(0.1), 
                                      strides=[4, 4])
            print(self.n_out.get_shape())
#            self.n_out = tf.Print(self.n_out,['>>>>>',tf.reduce_sum(self.n_out[0]-self.n_out[-1])], summarize=1000) 
            self.n_out = tf.layers.conv2d(self.n_out, 
                                      filters=64, 
                                      kernel_size=[4, 4], 
                                      name='{}'.format(2), 
                                      padding=padding,
                                      activation=getattr(tf.nn, 'relu', None),
                                      kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
                                      bias_initializer=tf.constant_initializer(0.1), 
                                      strides=[2, 2])

            self.n_out = tf.layers.conv2d(self.n_out, 
                                      filters=64, 
                                      kernel_size=[3, 3], 
                                      name='{}'.format(3), 
                                      padding=padding,
                                      activation=getattr(tf.nn, 'relu', None),
                                      kernel_initializer=tf.truncated_normal_initializer(stddev=0.02),
                                      bias_initializer=tf.constant_initializer(0.1), 
                                      strides=[1, 1])

            self.n_out = tf.layers.flatten(self.n_out)
            self.n_out = tf.layers.dense(self.n_out, 
                                units=512, 
                                activation=getattr(tf.nn, 'relu', None),
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.02),
                                bias_initializer=tf.constant_initializer(0.1), 
                                name='dense1')     
            self.n_out = tf.layers.dense(self.n_out, 
                                activation=getattr(tf.nn, self.activation, None),
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.02),
                                bias_initializer=tf.constant_initializer(0.1), 
                                units=self.env.action_space.n, 
                                name='dense2')

        with tf.variable_scope('{}/'.format(self.namespace)):
            self.n_out = tf.identity(self.n_out, name='n_out')
            tf.summary.histogram('n_out', self.n_out)
            self.out_prob = tf.nn.softmax(self.n_out, name='out_prob')
            tf.summary.histogram('out_prob', self.out_prob)
        
     
