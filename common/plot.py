import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import cv2, os
import numpy as np
import utils

fig = None
ROOT_IMG_FOLDER = 'images'
def genImage(img, shape):
    return np.array(img).reshape(*shape)

def genCnnImg(images):
    width, height, channels = images.shape
    data = []
    for ch in  range(channels):
        data.append(images[:,:,ch])
    return data

def genFeatureImg(images):
    batch, width, height, channels = images.shape
    print('image.shape:{}'.format(images.shape))
    batch_data = []
    for index,img in enumerate( images):
        print('img.shape:{}'.format(img.shape))

        batch_data.append((index, genCnnImg(img)))
    return batch_data

def saveFeature(folder, layer ,images):
    path = os.path.join(os.getcwd(), ROOT_IMG_FOLDER, folder, layer)
    utils.check_path(path)
    data = genFeatureImg(images)
    for bth_dex, imgs in data:
        for f_inx, img in enumerate(imgs):
            cv2.imwrite(os.path.join(path, '{}-{}.jpg'.format(bth_dex, f_inx)), img)    

def genFilterImg(images):
    width, height, channels, size = images.shape
    print( width, height, channels, size )
    data = []
    for s in range(size):
        for ch in  range(channels):
            data.append(images[:,:,ch, s])
    return data

def saveFilter(folder, images):
    path = os.path.join(os.getcwd(), ROOT_IMG_FOLDER, folder)
    utils.check_path(path)
    data = genFilterImg(images)
    for index, img in enumerate(data):
        cv2.imwrite(os.path.join(path, '{}.jpg'.format(index)), img)    
        
def plotFigures(data, culNum = 5, labels=[], realname=[], panel=True, save_path=None):
    global fig
    if fig == None:
       #fig = plt.figure()
       flg = plt.figure(figsize=(50,50))
    data_length = len(data)
    rowNum = int(data_length/culNum) + 1
    gs = gridspec.GridSpec(rowNum,culNum)
    row = -1
    for index, dt in enumerate(data):
        col = index % culNum
        if col == 0:
           row = row + 1
        ax = plt.subplot(gs[row, col])
        plt.tight_layout()
        if(len(realname) > 0 and len(realname) == data_length):
            plt.xlabel(realname[index])
        if (len(labels) > 0 and  len(labels) == data_length):
            plt.title(labels[index])
        vmin, vmax = utils.pixel_range(dt)
        plt.imshow(dt, vmin=vmin, vmax=vmax)
    if panel:
        plt.show(block = False)   
        _ = input("Press [enter] to continue.")
        #print( _)
        plt.clf()
    if save_path:
        plt.savefig(save_path, bbox_inches='tight')

if __name__ == "__main__":
   data = [[[1,2],[2,1]] for i in range(32)]
   plotFigures(data, panel=False, save_path='./test.jpg')
