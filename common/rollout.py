import __init__
import time
from common.result_base import ResultSummary

class RollOutBase(object):
    def __init__(self, maxstep, clear, render, sleep, aug_rew, agent, env, summary=None):
        self.maxstep = maxstep
        self.summary = None
        self._render = render
        self._clear = clear
        self._sleep = sleep 
        self.aug_rew = aug_rew
        self.env = env
        self.agent = agent
        if summary == None:
            self.summary = ResultSummary()
        else:
            self.summary = summary

    def reset_env(self):
        self.env.reset() 

    def sleep(self, sleep):
        self._sleep = sleep

    def clear(self, clear):
        self._clear = clear
 
    def render(self, render):
        self._render = render

    def summary_reset(self):
        self.summary.reset_hist()      

    def get_result(self):
        return self.summary.cal_result()

    def sample(self, env, agent, need_hist):
        raise NotImplementedError("go is not implemented")

    def go(self, need_hist=False):
        obs, rew, done = self.env.reset(), 0, False
        obses, actses, rewses = [], [], []
        step=0
        while not done:
            act = None
            if self.agent is None:
                act = self.env.action_space.sample()
            else:
                act = self.agent.get_acts([obs])
            obs_, rew, done, info = self.env.step(act)
            if self._render:
                self.env.render()
            if self._clear:
                os.system('clear')
            time.sleep(self._sleep)
            actses.append(act)
            obses.append(obs)
            rewses.append(rew)
            obs = obs_
            self.summary.set_summary_each_step(act=act, obs=obs, rew=rew)
            if done:
               self.summary.set_summary(done, rew)
            step+=1
            if step > self.maxstep:
               print('max step:{} is reached'.format(self.maxstep))
               break
        if need_hist:
            print(self.summary.get_hist())

        return obses, actses, rewses

class RollOutBaseOld(object):
    def __init__(self, maxstep, clear, render, sleep):
        self.maxstep = maxstep
        self.summary = None
        self._render = render
        self._clear = clear
        self._sleep = sleep 

    def reset_env(self):
        self.env.reset() 
    def sleep(self, sleep):
        self._sleep = sleep

    def clear(self, clear):
        self._clear = clear
 
    def render(self, render):
        self._render = render


    def summary_reset(self):
        self.summary.reset_hist()      

    def get_result(self):
        return self.summary.cal_result()

    def go(self, env, agent=None, need_hist=False):
        raise NotImplementedError("go is not implemented")






