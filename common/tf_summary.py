import tensorflow as tf

class Summary(object):
    def __init__(self, logs_path):
        self.path =logs_path
        self.writer = None

    def init_writer(self):
        self.writer = tf.summary.FileWriter(self.path, graph=tf.get_default_graph())
       
    def add_summary(self, *args):
        self.writer.add_summary(*args)

    def get_summary_merge(self, key):
        return tf.summary.merge_all(key='summaries', scope=key)

