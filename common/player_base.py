
class PlayerBase(object):
    def __init__(self,env, agent, gamma, render, rollout, play_round, round_mgt):
        self.env = env
        self.agent = agent
        self.gamma = gamma
        self.render = render
        self.rollout = rollout
        self.play_round=play_round
        self.round_mgt = round_mgt

    def play(self, need_load, round):
        if need_load:
            self.agent.load()
        self.rollout.sleep(0)
        self.rollout.clear(False)
        self.rollout.render(self.render)
        print('start to play')
        self.rollout.summary_reset() 
        for i in range(round):
            self.rollout.go(need_hist=True)
        self.env.close()

    def get_discount_sum_of_rewards(self, rewards):
        raise NotImplementedError("get_discount_sum_of_rewards is not implemented")

    def train(self, epoch, play_round):
        raise NotImplementedError("train is not implemented")

