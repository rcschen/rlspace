import __init__

class ResultSummaryBase(object):
    def __init__(self):
        self.hist = {}
        self.reset_hist()

    def cal_result(self):
        raise NotImplementedError("cal_result is not implemented")

    def reset_hist(self):
        raise NotImplementedError("reset_hist is not implemented")

    def set_summary(self, env, done, rew):
        raise NotImplementedError("get_summary is not implemented")

    def get_hist(self):
        return self.hist       

    def set_summary_each_step(self, *args, **kwargs):
        raise NotImplementedError('Not Implemented Method')

class ResultSummary(ResultSummaryBase):
    def __init__(self):
        super(ResultSummary, self).__init__()

    def cal_result(self):
        return float(self.hist['G'] / (self.hist['G'] + self.hist['H']))

    def reset_hist(self):
        self.hist = {'G':0, 'H':0}
 
    def set_summary(self, done, rew):
        if done:
            if rew <= 0:
                self.hist['H'] += 1
                print('T____________T')
            else:
                self.hist['G'] += 1
                print('>____________^')

    def set_summary_each_step(self, **kwargs):
        pass

