import tensorflow as tf
from gym.spaces import Discrete, Box, MultiBinary, MultiDiscrete

def get_ph_by_env(ob_space, name):
    if isinstance(ob_space, Discrete):
        observation_ph = tf.placeholder(shape=(None,), dtype=tf.int32, name=name)
        observation_ph_oh = tf.reshape(observation_ph,(-1,1))
        processed_observations = tf.cast(tf.one_hot(observation_ph, ob_space.n), tf.float32) # tf.one_hot(observation_ph, ob_space.n)
        return observation_ph, processed_observations

    elif isinstance(ob_space, Box):
        print('???????????????????/',ob_space.dtype)
        observation_ph = tf.placeholder(shape=(None, ) + ob_space.shape, dtype=ob_space.dtype, name=name)
        print('ooooooooooooo',observation_ph)
        processed_observations = tf.cast(observation_ph, tf.float32)
        '''
        # rescale to [1, 0] if the bounds are defined
        if (scale and
           not np.any(np.isinf(ob_space.low)) and not np.any(np.isinf(ob_space.high)) and
           np.any((ob_space.high - ob_space.low) != 0)):

            # equivalent to processed_observations / 255.0 when bounds are set to [255, 0]
            processed_observations = ((processed_observations - ob_space.low) / (ob_space.high - ob_space.low))
        '''
        return observation_ph, processed_observations

    elif isinstance(ob_space, MultiBinary):
        raise NotImplementedError("Error: the model does not support input space of type {}".format(type(ob_space).__name__))   

    elif isinstance(ob_space, MultiDiscrete):
        raise NotImplementedError("Error: the model does not support input space of type {}".format(type(ob_space).__name__))   
    else:
        raise NotImplementedError("Error: the model does not support input space of type {}".format(type(ob_space).__name__))
