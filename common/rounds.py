class RoundMgt(object):
    def __init__(self, types=['save', 'video']):
        self.best_reward=-10000
        self.last_reward = self.best_reward
        self.notify_types = types
        self.notifications = {}
        self.set_notification(False)
        self.best_round=0

    def set_notification(self, flag):
        for tpy in self.notify_types:
            self.notifications[tpy] = flag 
                  
    def calculate_best_reward(self, round_record, round_idx=0):
        self.last_reward = sum(round_record) 
        if self.last_reward >= self.best_reward:
            self.best_reward = self.last_reward
            self.best_round=round_idx
            self.set_notification(True)
            print('best set all true: {}'.format(self.notifications))
        print('round reward: {}, the best is: {} at {}'.format(self.last_reward, self.best_reward, self.best_round))

    def get_notification(self, nType):
        if not nType in self.notifications.keys():
            raise Exception('NO KEY EXISTS: {}'.format(nType))
        value = self.notifications[nType]
        self.notifications[nType] = False
        print('{} get the notification: {}'.format(nType, self.notifications))
        return value
