
import gym
import numpy as np
import time
import pickle, os

env = gym.make('FrozenLake-v0')

with open("frozenLake_qTable.pkl", 'rb') as f:
	Q = pickle.load(f)

def choose_action(state):
	action = np.argmax(Q[state, :])
	return action
result = {'s':0, 'f':0}
# start
for episode in range(100):

	state = env.reset()
	print("*** Episode: ", episode)
	t = 0
	while t < 1000:

		action = choose_action(state)  
		
		state2, reward, done, info = env.step(action)  
		
		state = state2
		env.render()

		if done:
			if reward == 1:
				result['s'] +=1
			else:
				result['f'] +=1
			break

		time.sleep(0.5)
		os.system('clear')
print(result)
