import __init__
import numpy as np
import gym, time, os, argparse
from gym import wrappers
from gym import register
import random as pr
from utils import save, load, evaluate_policy, check_path
from fl88env import FLEnv
from cp_common import get_action

def extract_policy(q, gamma = 1.0):
    """ Extract the policy given a value-function """
    policy = np.zeros(env.nS)
    for s in range(env.nS):
        policy[s] = np.argmax(q[s])
    return policy


EPISODES = 50000
ACT_TYPE=1

def get_qtable(env): 
    qtable = np.zeros((env.observation_space.n, env.action_space.n))
    sa_values = np.zeros(qtable.shape)
    sa_count = np.ones(qtable.shape)
    returns = {}
    for i in range(EPISODES):
        s = env.reset()
        complete = False
        history = []
        G = 0
        while complete == False:
            action = get_action(qtable, s, ACT_TYPE)
            s_, reward, complete, _ = env.step(action)
#            print('s:{} >>>> s_:{}'.format(s, s_))

            history.append((action, s, reward))
            s = s_
 #       print('history:{}'.format(history))
        for j in reversed(range(0, len(history))):
            ha, hs, sr = history[j]
            if ha==0 and hs ==0 and sr == 1:
                print('##################')
                print('##################')
                print('##################')
                print('##################')
                print('##################')

                print('hs:{}, ha:{}, sr:{}'.format(hs, ha,sr))
                print('##################')

            G += sr
            if not (ha, hs) in [(h[0], h[1]) for h in history[0:j]]:
                if returns.get((ha, hs)):
                    returns[(ha, hs)].append(G)
                else:
                    returns[(ha, hs)] = [G]
#                print('ha:{}, hs:{}'.format(ha, hs))
#                print('returns:{}'.format(returns))
                qtable[hs, ha] = sum(returns[(ha, hs)]) / len(returns[(ha, hs)])
        if hs == 0:
            print('------------------')

#            print('returns:{}'.format(returns[(0,0)]))
            print('{}: qtable:{}'.format(i,qtable[0]))
            #print('sum[return]:{}'.format(sum(returns[(0,0)])))
            print('tracer:{}'.format(history))
            print('------------------')
#            if sum(returns[(0,0)])==1:
#                input('wart....')
    return qtable
            

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t') 
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    PATH = check_path( os.path.join('__d_frozenlake',__file__.split('.')[0]))
    PATH = os.path.join(PATH, 'qtable')
    register(
            id='FrozenLakeNotSlippery-v0',
            entry_point='gym.envs.toy_text:FrozenLakeEnv',
            kwargs={'map_name' : '4x4', 'is_slippery': False},
            )
    #env_name  = 'FrozenLake8x8-v0'
    env_name  = 'FrozenLake-v0'
#    env_name = 'FrozenLakeNotSlippery-v0'
    gamma = 1.0
    env = gym.make(env_name)
    env = env.unwrapped
#    fe = FLEnv()
#    env = fe.get_env()

    if args.td == 'train': 
        q = get_qtable(env)
        print('q:{}'.format(q))        
        policy = extract_policy(q)
        save(policy, PATH)
    policy = load(PATH)
    policy_score = evaluate_policy(env, policy, gamma, n=1000)
    print('Policy average score = ', policy_score)
