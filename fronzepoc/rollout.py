import __init__
import time
from common_base import ResultSummaryBase, RollOutBase
from common_base import AugmentReward as AugmentRewardBase


class ResultSummary(ResultSummaryBase):
    def __init__(self):
        super(ResultSummary, self).__init__()

    def cal_result(self):
        return float(self.hist['G'] / (self.hist['G'] + self.hist['H']))

    def reset_hist(self):
        self.hist = {'G':0, 'H':0}
 
    def set_summary(self, done, rew):
        if done:
            if rew <= 0:
                self.hist['H'] += 1
                print('T____________T')
            else:
                self.hist['G'] += 1
                print('>____________^')

class PGRollOut(RollOutBase):
    def __init__(self, maxstep=500, clear=False, render=False, sleep =0, aug_rew=None, agent=None, env=None):
        super(PGRollOut, self).__init__(maxstep, clear, render, sleep, aug_rew, agent, env)
        self.summary = ResultSummary()

    def sample(self):
        obs, rew, done, info = self.env.reset(), 0 , False, {}
        obses, acts, rews, dones = [], [], [], []
        while not done:
            act = self.agent.get_acts([obs])
            obs_, rew, done, info = self.env.step(act)
            if self.aug_rew:
                rew = self.aug_rew.get_reward(done=done, 
                                              rew=rew, 
                                              obs=obs, 
                                              obs_=obs_,
                                              rew_length=len(rews))
            acts.append(act)
            obses.append(obs)
            rews.append(rew)
            dones.append(done)
            obs = obs_
        return obses, acts, rews, dones

    def go(self, need_hist=False):
        obs, rew, done = self.env.reset(), 0, False
        obses, actses, rewses = [], [], []
        step=0
        while not done:
            act = None
            if self.agent is None:
                act = self.env.action_space.sample()
            else:
                act = self.agent.get_acts([obs])
            obs_, rew, done, info = self.env.step(act)
            if self._render:
                self.env.render()
            if self._clear:
                os.system('clear')
            time.sleep(self._sleep)
            actses.append(act)
            obses.append(obs)
            rewses.append(rew)
            obs = obs_
            if done:
               self.summary.set_summary(done, rew)
            if done and need_hist:
               print(self.summary.get_hist())
            step+=1
            if step > self.maxstep:
               print('max step:{} is reached'.format(self.maxstep))
               break
        return obses, actses, rewses


class A2CRollOut(PGRollOut):
    def __init__(self, maxstep=500, clear=False, render=False, sleep =0, aug_rew=None, agent=None, env=None):
        super(A2CRollOut, self).__init__(maxstep, clear, render, sleep, aug_rew, agent, env)

    def sample(self):
        obs, rew, done, info = self.env.reset(), 0 , False, {}
        obses, acts, rews, dones, obses_ = [], [], [], [], []
        while not done:
            act = self.agent.get_acts([obs])
            obs_, rew, done, info = self.env.step(act)
            if self.aug_rew:
                rew = self.aug_rew.get_reward(done=done, 
                                              rew=rew, 
                                              obs=obs, 
                                              obs_=obs_,
                                              rew_length=len(rews))
            acts.append(act)
            obses.append(obs)
            rews.append(rew)
            dones.append(done)
            obses_.append(obs_)
            if done and rew==1:
                print('>>>>>>>>>>>>',obses)
                print('-----------',obses_) 

            obs = obs_
        return obses, acts, rews, dones, obses_


class QRollOut(PGRollOut):
    def __init__(self, maxstep=500, clear=False, render=False, sleep =0, aug_rew=None, agent=None, env=None):
        super(QRollOut, self).__init__(maxstep, clear, render, sleep, aug_rew, agent, env)

    def sample(self, obs):
        act = self.agent.get_acts([obs])
        obs_, rew, done, info = self.env.step(act)
        if self.aug_rew:
            rew = self.aug_rew.get_reward(done=done, 
                                          rew=rew, 
                                          obs=obs, 
                                          obs_=obs_)
        return obs, act, obs_,  rew, done

class AugmentReward(AugmentRewardBase):
    def __init__(self, selector, rew_scale):
        super(AugmentReward, self).__init__(rew_scale)
        self.func = getattr(self,'ar_{}'.format(selector))

    def ar_0(self, **kwargs):
        return  self.rew_scale*kwargs.get('rew')

    def ar_1(self, **kwargs):
        if kwargs.get('done') :
            if kwargs.get('rew') == 1:
                return self.rew_scale*1
            else:
                return self.rew_scale*(-1)
        else:
            return kwargs.get('rew')
   
    def ar_2(self, *args, **kwargs):
        if kwargs.get('done') :
            if kwargs.get('rew') == 1:
                return self.rew_scale*kwargs.get('rew')
            else:
                return -1*self.rew_scale*kwargs.get('rew')
        else:
            if kwargs.get('obs') == kwargs.get('obs_'):
                return -2
            else:
               return 2

    def ar_3(self, *args, **kwargs):
        if kwargs.get('rew_length')>30:
            return 0
        return self.rew_scale*kwargs.get('rew')

    def ar_4(self, *args, **kwargs):
        if kwargs.get('rew_length')>30:
            return -1*self.rew_scale
        return self.rew_scale*kwargs.get('rew')

    def ar_5(self, **kwargs):
        if kwargs.get('rew_length')>30:
            return -1*self.rew_scale
        if kwargs.get('done') :
            if kwargs.get('rew') == 1:
                return self.rew_scale*1
            else:
                return self.rew_scale*(-1)
        else:
            return kwargs.get('rew')

    def ar_6(self, **kwargs):
        if kwargs.get('rew_length')>30:
            return -1*kwargs.get('rew_length')
        if kwargs.get('done') :
            if kwargs.get('rew') == 1:
                return self.rew_scale*1
            else:
                return self.rew_scale*(-1)
        else:
            return 1

