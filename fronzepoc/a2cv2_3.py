'''
no slip vnet activation None: 
> python a2cv2_3.py --type train --cont f --greedy_denom 50000 --learn_rate 0.0001 --need_greedy true --play_load "" --play_render "" --play_round 1000 --gamma 1 --train_epoch 50000 --reward_type 5  --slip "" --det_action true --activation tanh --rew_scale 1
slip 40% vnet activation tanh
> python a2cv2_3.py --type train --cont f --greedy_denom 50000 --learn_rate 0.001 --need_greedy true --play_load "" --play_render "" --play_round 1000 --gamma 1 --train_epoch 50000 --reward_type 5  --slip true --one_det true --det_action true --activation tanh --rew_scale 1 
'''
import __init__
import numpy as np
import os, time, argparse, random
from utils import check_path
from rollout import A2CRollOut, AugmentReward
from cp_common import Env
from a2cv2_2 import Agent
from a2cv2_2 import Player as PlayerParent

logs_path = os.path.join('__d_logs',__file__.split('.')[0], str(int(time.time())))
PATH = check_path( os.path.join('__d_model',__file__.split('.')[0]))
PATH = os.path.join( PATH, 'model')


class Player(PlayerParent):
    def __init__(self,env, agent, gamma, render, rollout, data_len, batch_size):
        super(Player, self).__init__(env=env,
                                            agent=agent,
                                            gamma=gamma,
                                            render=render,
                                            rollout=rollout,
                                            data_len=data_len,
                                            batch_size=batch_size)

    def train(self, epoch=2000):
        print('start to train')
        for i in range(epoch):
            obses, acts, rews, dones, obses_ = self.rollout.sample()
            print('============================================ {} ===== {} =======> {}'.format(i, rews[-1], self.agent.ep, rews[-1]))
            diffQ = self.agent.train_critic(obses=obses, 
                                            acts=acts, 
                                            obses_=obses_,
                                            rewards=rews, 
                                            dones=dones)
            self.agent.train_actor(obses=obses, acts=acts, rews=diffQ)
            self.agent.plus_loop()
            if i > 1000 and  i % 500 == 0:
                 self.play(need_load=False, round=100)
#                if self.rollout.get_result() >= 0.0:
                 self.agent.save()
        self.agent.close_session()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--type",type=str, help="train or play", default='play')
    parser.add_argument("-c", "--cont", type=str, help="continue exist table", default='t')
    parser.add_argument("-e", "--greedy_denom", type=int, help="greedy_denom", default=10000)
    parser.add_argument("-l", "--learn_rate", type=float, help="learning rate", default=0.0001)
    parser.add_argument("-g", "--need_greedy", type=bool, help="need greedy", default=False)
    parser.add_argument("-i", "--play_load", type=bool, help="player need load", default=True)
    parser.add_argument("--play_render", type=bool, help="player render", default=False)
    parser.add_argument("--play_round", type=int, help="player round", default=1000)
    parser.add_argument("--play_clear", type=bool, help="player clear", default=False)
    parser.add_argument("--play_sleep", type=int, help="player sleep", default=0)
    parser.add_argument("--slip", type=bool, help="slipping", default=True)
    parser.add_argument("--one_det", type=bool, help="one deterministic", default=False)
    parser.add_argument("--gamma", type=float, help="gamma", default=0.99)
    parser.add_argument("--train_epoch", type=int, help="train epoch", default=10000)
    parser.add_argument("--reward_type", type=int, help="reward type", default=0)
    parser.add_argument("--det_action", type=bool, help="deterministic action", default=True)
    parser.add_argument("--activation", type=str, help="activation function", default='relu')
    parser.add_argument("--rew_scale", type=int, help="rew_scale", default=1)
    parser.add_argument("--data_len", type=int, help="data length", default=1000)
    parser.add_argument("--batch_size", type=int, help="batch size", default=50)
    parser.add_argument("--optimizer", type=str, help="optimizer", default='ada')

    args = parser.parse_args()
    print(args)
    isContinue = (args.cont in ['t','true','True', '1'])
    print('=================================================')
    print(args)
    print('=================================================')
    print('isContinue',isContinue)
    env = Env(slip=args.slip, one_det=args.one_det).get_env() 
    agent = Agent(env=env, 
                  model_path=PATH, 
                  logs_path=logs_path, 
                  debug=False, 
                  namespace='qnn', 
                  gd=args.greedy_denom, 
                  learnrate=args.learn_rate,
                  det_action=args.det_action,
                  activation=args.activation,
                  need_greedy=args.need_greedy,
                  gamma=args.gamma,
                  optimizer=args.optimizer)
    if not isContinue:
        agent.init_session()
    else:
        agent.load()
    aug_rew = AugmentReward(selector=args.reward_type, rew_scale=args.rew_scale)
    rollout = A2CRollOut(maxstep=500, 
                        clear=args.play_clear,
                        render=args.play_render,
                        sleep=args.play_sleep,
                        aug_rew=aug_rew,
                        agent=agent,
                        env=env)
    player = Player(env=env, 
                    agent=agent,
                    gamma=args.gamma,
                    render=args.play_render,
                    rollout=rollout,
                    data_len=args.data_len, 
                    batch_size=args.batch_size)
    if args.type == 'train':
        player.train(epoch=args.train_epoch)
    elif args.type == 'play':
        player.agent.set_need_greedy(need_greedy=False)
        player.play(need_load=args.play_load, 
                    round=args.play_round)
    else:
        print('No options')
    env.close()        

