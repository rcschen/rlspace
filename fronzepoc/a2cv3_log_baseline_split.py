'''
python a2cv3_log_baseline_split.py --type train --cont f --greedy_denom 50000 --learn_rate 0.0007 --need_greedy true --play_load "" --play_render "" --play_round 1000 --gamma 0.99 --train_epoch 200000 --reward_type 0  --slip true --det_action true  --activation tanh --rew_scale 1 --optimizer rms --one_det "
'''

import __init__
import copy
import numpy as np
import os, time, random
from utils import check_path
from rollout import A2CRollOut, AugmentReward
from cp_common import np_one_hot, Env
from argment_fl import ArgmentA2C as AC
from a2cv3_1 import Agent as A2CV31
from a2cv2_2 import Player as PlayerParent
from net_base import A2CNetStableBaseLineSplit
logs_path = os.path.join('__d_logs',__file__.split('.')[0], str(int(time.time())))
PATH = check_path( os.path.join('__d_model',__file__.split('.')[0]))
PATH = os.path.join( PATH, 'model')


class Agent(A2CV31):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, gamma, optimizer):
        super(Agent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy,
                                    gamma=gamma,
                                    optimizer=optimizer)
        self.net = A2CNetStableBaseLineSplit(env=env, 
                          namespace=namespace, 
                          learnrate=learnrate, 
                          activation=activation, 
                          critic_ns = self.critic_ns, 
                          actor_ns = self.actor_ns,
                          optimizer=optimizer,
                          v_coef=0.25, 
                          ent_coef=0.01)


    def init_summary(self):
        super().init_summary()
        self.sm_namespace = self.sm.get_summary_merge(self.namespace)
        self.success_namespace = self.sm.get_summary_merge('success'.format(self.namespace))

    def set_result(self, success):
        print(np.array([success]).shape)
        feed_dict = {
            self.net.success : success
        }
        res = self.sess.run(self.success_namespace, feed_dict=feed_dict)
        self.sm.add_summary(res, self.loop)     
           
    def train(self, obses, acts, obses_, rewards, dones):
        feed_dict = {
            self.net.obses : np_one_hot(obses, self.env.observation_space.n),
            self.net.acts: np.array(acts),
            self.net.target: np.array(self.get_critic_target(obses_, rewards, dones)),
        }
        diffq = self.sess.run(self.net.diffQ, feed_dict=feed_dict)
        #print('!!!!!',diffq)
        feed_dict[self.net.rews] = diffq
        #print(feed_dict)
        opt, entropy, loss, closs, aloss, sm_namespace = self.sess.run([self.net.optimize,
                                                 self.net.entropy, 
                                                 self.net.total_loss, 
                                                 self.net.critic.loss,
                                                 self.net.actor.loss,
                                                 self.sm_namespace], feed_dict=feed_dict)

        #print('?????????????????????????????????????????????',entropy)
        #print('?????????????????????????????????????????????',loss)
        #print('?????????????????????????????????????????????closs',closs)
        #print('?????????????????????????????????????????????aloss',aloss)

        self.sm.add_summary(sm_namespace, self.loop)     

    def get_critic_target(self, obses, rews, dones):
        target = []
#        print('get_critic_target: obses:{}'.format(obses))
        v = rews[-1]
#        if dones[-1] and rews[-1] == 0:
#            v = self.get_critic_v([obses[-1]])[0,0]
#        print('get_critic_target: v:{}'.format(v))
#        print('get_critic_target: >>>> rews: {}'.format(rews))
#        print('get_critic_target: <<<< v:{}'.format(rews.reverse()))
        for rew, done in zip(rews[::-1], dones[::-1]):
#            print(v)
#            print(done)
            if done:
                target.append(v)
            else:
                v = rew + self.gamma* v #*(0 if done else 1)
                target.append(v)
        print('get_critic_target: >>>>target:{}'.format(target[::-1]))
        return target[::-1]


    def get_critic_target1(self, obses, rews, dones):
        target = []
        print('get_critic_target: obses:{}'.format(obses))

        v = self.get_critic_v([obses[-1]])[0,0]
#        print('get_critic_target: v:{}'.format(v))
        print('get_critic_target: >>>> rews: {}'.format(rews))
#        print('get_critic_target: <<<< v:{}'.format(rews.reverse()))
        for rew, done in zip(rews[::-1], dones[::-1]):
            print(v)
            print(done)
            if done:
                target.append(v)
            else:
                v = rew + self.gamma* v #*(0 if done else 1)
                target.append(v)
        print('get_critic_target: >>>>target:{}'.format(target[::-1]))
        return target[::-1]

class Player(PlayerParent):
    def __init__(self,env, agent, gamma, render, rollout, data_len, batch_size):
        super(Player, self).__init__(env=env,
                                            agent=agent,
                                            gamma=gamma,
                                            render=render,
                                            rollout=rollout,
                                            data_len=data_len,
                                            batch_size=batch_size)

    def train(self, epoch=2000):
        print('start to train')
        for i in range(epoch):
            obses, acts, rews, dones, obses_ = self.rollout.sample()
            print('==================================================================================== {} ===== {} =======> {}'.format(i, rews[-1], self.agent.ep, rews[-1]))
            print(obses)
            diffQ = self.agent.train(obses=obses, 
                                            acts=acts, 
                                            obses_=obses_,
                                            rewards=rews, 
                                            dones=dones)
            self.agent.plus_loop()
            if i > 1000 and  i % 500 == 0:
                 self.play(need_load=False, round=100)
#                if self.rollout.get_result() >= 0.0:
                 self.agent.save()
                 print(self.rollout.get_result()) 
                 self.agent.set_result(self.rollout.get_result())
        self.agent.close_session()

if __name__ == "__main__":
    args = AC().get_args()
    print(args)
    isContinue = (args.cont in ['t','true','True', '1'])
    print('=================================================')
    print(args)
    print('=================================================')
    print('isContinue',isContinue)
    env = Env(slip=args.slip, one_det=args.one_det).get_env() 
    agent = Agent(env=env, 
                  model_path=PATH, 
                  logs_path=logs_path, 
                  debug=False, 
                  namespace='qnn', 
                  gd=args.greedy_denom, 
                  learnrate=args.learn_rate,
                  det_action=args.det_action,
                  activation=args.activation,
                  need_greedy=args.need_greedy,
                  gamma=args.gamma,
                  optimizer=args.optimizer)
    if not isContinue:
        agent.init_session()
    else:
        agent.load()
    aug_rew = AugmentReward(selector=args.reward_type, rew_scale=args.rew_scale)
    rollout = A2CRollOut(maxstep=500, 
                        clear=args.play_clear,
                        render=args.play_render,
                        sleep=args.play_sleep,
                        aug_rew=aug_rew,
                        agent=agent,
                        env=env)
    player = Player(env=env, 
                    agent=agent,
                    gamma=args.gamma,
                    render=args.play_render,
                    rollout=rollout,
                    data_len=args.data_len, 
                    batch_size=args.batch_size)
    if args.type == 'train':
        player.train(epoch=args.train_epoch)
    elif args.type == 'play':
        player.agent.set_need_greedy(need_greedy=False)
        player.play(need_load=args.play_load, 
                    round=args.play_round)
    else:
        print('No options')
    env.close()        

