import __init__
import gym, argparse
import os, time
import tensorflow as tf
LEARNING_RATE=1e-2
import numpy as np
from utils import get_summary 
from gym import wrappers
from gym import register

hist = {'G':0, 'H':0}
logs_path = os.path.join('./pglog',str(int(time.time())))

def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    init_np[np.arange(len(data)), data] = 1
    return init_np

class Agent(object):
    def __init__(self, env, model_path='model'):
        self.env = env
        self.obses = None
        self.acts = None
        self.rews = None
        self.out_prob = []
        self.optimize = None
        self.sess = None
        self.saver = None
        self.model_path = model_path
        self.loss = None

    def init_session(self):
        self.net()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)

    def close_session(self):
        if self.sess is not None:
            self.sess.close()

    def save(self):
        self.saver = tf.train.Saver()
        self.saver.save(self.sess, self.model_path)   

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.out_prob = tf.get_default_graph().get_tensor_by_name('out_prob:0')
        self.obses = tf.get_default_graph().get_tensor_by_name('obses:0')

    def net(self):
        self.obses = tf.placeholder(tf.float32, shape=[None, 1],name='obses')
        self.acts = tf.placeholder(tf.int32, name='acts')
        self.rews = tf.placeholder(tf.float32, name='rews')
        n_out = tf.layers.dense(self.obses, 
                                units=36, 
#                                activation=tf.nn.relu, 
                                activation=tf.nn.tanh,  # tanh activation
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                name='dense1')         
        n_out = tf.layers.dense(n_out, 
                                activation=tf.nn.tanh,  # tanh activation
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                units=self.env.action_space.n, 
                                name='dense2') 

        self.out_prob = tf.nn.softmax(n_out, name='out_prob')
        '''
        with tf.name_scope('loss'):
            # to maximize total reward (log_p * R) is to minimize -(log_p * R), and the tf only have minimize(loss)
            neg_log_prob = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=all_act, labels=self.tf_acts)   # this is negative log of chosen action
            # or in this way:
            # neg_log_prob = tf.reduce_sum(-tf.log(self.all_act_prob)*tf.one_hot(self.tf_acts, self.n_actions), axis=1)
            loss = tf.reduce_mean(neg_log_prob * self.tf_vt)  # reward guided loss
        '''
        self.out = tf.reduce_sum(-tf.log(self.out_prob)*tf.one_hot(self.acts, self.env.action_space.n), axis=1) 
        self.loss = tf.reduce_mean(self.out * self.rews)
        optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
        self.optimize = optimizer.minimize(self.loss)
        tf.summary.scalar('loss', self.loss)
        self.summary = tf.summary.merge_all()
        self.writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())

    def get_acts(self, obses):
        if self.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.obses: np.array(obses).reshape(-1,1)}
        return self.sess.run(self.out_prob, feed_dict = feed_dict)

    def train(self, obses, acts, rews, times):
        feed_dict = {
            self.obses : np.array(obses).reshape((-1,1)),
            self.acts: acts,
            self.rews: rews
        }
        opt, loss,sm, out = self.sess.run([self.optimize, self.loss, self.summary, self.out], feed_dict=feed_dict)
#        print('out:{}'.format(out))
#        print('loss:{}'.format(loss))
        self.writer.add_summary(sm, times)


def rollout(env, agent=None, clear=False, render=False):
    obs, rew, done = env.reset(), 0, False
    obses, actses, rewses = [], [], []
    while not done:
        if agent is None:
            obs_, rew, done, info = env.step(env.action_space.sample())
        else:
            prob = np.squeeze(agent.get_acts([obs]), axis=0)
            act_space = np.arange(env.action_space.n)
            act = np.random.choice(act_space, p=prob)
            obs_, rew, done, info = env.step(act)
        obses.append(obs)
        actses.append(act)
        rewses.append(rew)
        print(get_summary(env, done, rew, render, clear ))
        obs=obs_
    print('total rewses: {}'.format(sum(rewses)))
    return obses, actses, rewses

class Player(object):
    def __init__(self,env, agent=None):
        self.env = env
        self.agent = agent

    def play(self):
        self.agent.load()
        print('start to play')
        for i in range(100):
            print('############################', i)
            rollout(self.env, self.agent, False, True)
        self.env.close()

    def get_discount_sum_of_rewards(self, rewards, gamma=0.99):
#        print(rewards)
        adv = np.zeros_like(rewards)
#        print(adv)
        c = 0
        for i, r in enumerate(rewards):
            c = r + gamma *c
            adv[i] = c
        adv -= np.mean(adv)
        adv /= (np.std(adv) + 1e-10)
        return adv[::-1]

    def get_discount_sum_of_rewards1(self, rewards, gamma=0.99):
        print(rewards)
        adv = np.zeros_like(rewards)
        print(adv)
        c = 0
        for r in rewards:
            c = c + r
        for i, r in enumerate(rewards[::-1]):
            adv[i] = c
        return adv[::-1]

    def train(self, epoch=20000):
        print('start to train')
        self.agent.init_session()
        for i in range(epoch):
            print('=================== train round:',i)
            obses, acts, rews = rollout(self.env, self.agent, False, True)
#            print('obses: {}'.format(obses))
#            print('acts: {}'.format(acts))
#            print('rews: {}'.format(rews))
            print('len_rews: {}'.format(sum(rews)))
            dis_rews = self.get_discount_sum_of_rewards(rews)
#            print('dis_rew: {}'.format(dis_rews))
            self.agent.train(obses, acts, dis_rews, i)
            if i % 1000 == 0:
                self.agent.save()
        self.agent.close_session()
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    args = parser.parse_args()
    print(args.td)

    env = gym.make('FrozenLake-v0')    
    agent = Agent(env, 'frozenlake_pg/pg')
    player = Player(env, agent)
    getattr(player, args.td)()
    if args.td == 'train':
        player.play()
    env.close()         
