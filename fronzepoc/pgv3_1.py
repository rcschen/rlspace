'''
no slip: python pgv3_1.py --type train --cont f --greedy_denom 1000 --learn_rate 0.01 --need_greedy "" --play_load "" --play_render "" --play_round 1000 --gamma 0.99 --train_epoch 5000 --reward_type 0  --slip "" --det_action "" --activation relu

'''
import __init__
import argparse, os, time
import numpy as np
from cp_common import np_one_hot, Env
from rollout import PGRollOut
from utils import check_path
from agent_base import PGAgent
from rollout import AugmentReward
from player_base import PlayerBase

logs_path = os.path.join('__d_logs',__file__.split('.')[0], str(int(time.time())))
PATH = check_path( os.path.join('__d_model',__file__.split('.')[0]))
PATH = os.path.join( PATH, 'model')

class Agent(PGAgent):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy):
        super(Agent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy)

    def get_action(self, obses):
        if self.net.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.net.obses: np_one_hot(obses, self.env.observation_space.n)}
        prob,nout = self.sess.run([self.net.out_prob, self.net.n_out], feed_dict = feed_dict)
        if self.det_action:
            act = np.argmax(prob.ravel())
        else:
            act = np.random.choice(self.env.action_space.n, p=np.squeeze(prob, axis=0))
        return act

    def train(self, obses, acts, rews):
        feed_dict = {
            self.net.obses : np_one_hot(obses, self.env.observation_space.n),
            self.net.acts: acts,
            self.net.rews: rews
        }
        opt, loss,sm, out = self.sess.run([self.net.optimize, self.net.loss, self.sm_pg, self.net.n_out], feed_dict=feed_dict)
        self.sm.add_summary(sm, self.loop)       
        print('ep: {}'.format(self.ep))      


class Player(PlayerBase):
    def __init__(self,env, agent, gamma, render, rollout):
        super(Player, self).__init__(env, agent, gamma, render, rollout)

    def get_discount_sum_of_rewards(self, rewards):
        adv = np.zeros_like(rewards)
        c = 0
        for r in rewards:
            c = self.gamma*c + r
        for i, r in enumerate(rewards[::-1]):
            adv[i] = c
        return adv[::-1]

    def train(self, epoch):
        print('start to train')
        for i in range(epoch):
            obses, acts, rews, dones = self.rollout.sample()
            dis_rews = self.get_discount_sum_of_rewards(rewards=rews)
            print('========================================================================================= {} ============> {}'.format(i, rews[-1]))
            print('aaaaaaa: ',acts)
            self.agent.train(obses, acts, dis_rews)
            self.agent.plus_loop()
            if i > 1000 and  i % 500 == 0:
                 self.play(need_load=False, round=100)
#                if self.rollout.get_result() >= 0.0:
                 self.agent.save()
        self.agent.close_session()
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--type",type=str, help="train or play", default='play')
    parser.add_argument("-c", "--cont", type=str, help="continue exist table", default='t')
    parser.add_argument("-e", "--greedy_denom", type=int, help="greedy_denom", default=10000)
    parser.add_argument("-l", "--learn_rate", type=float, help="learning rate", default=0.0001)
    parser.add_argument("-g", "--need_greedy", type=bool, help="need greedy", default=False)
    parser.add_argument("-i", "--play_load", type=bool, help="player need load", default=True)
    parser.add_argument("--play_render", type=bool, help="player rebder", default=False)
    parser.add_argument("--play_round", type=int, help="player round", default=1000)
    parser.add_argument("--play_clear", type=bool, help="player clear", default=False)
    parser.add_argument("--play_sleep", type=int, help="player sleep", default=0)
    parser.add_argument("--slip", type=bool, help="slipping", default=True)
    parser.add_argument("--one_det", type=bool, help="one deterministic", default=False)
    parser.add_argument("--gamma", type=float, help="gamma", default=0.99)
    parser.add_argument("--train_epoch", type=int, help="train epoch", default=10000)
    parser.add_argument("--reward_type", type=int, help="reward type", default=0)
    parser.add_argument("--det_action", type=bool, help="deterministic action", default=True)
    parser.add_argument("--activation", type=str, help="activation function", default='relu')
    parser.add_argument("--rew_scale", type=int, help="rew_scale", default=1)

    args = parser.parse_args()
    print(args)
    isContinue = (args.cont in ['t','true','True', '1'])
    print('=================================================')
    print(args)
    print('=================================================')
    print('isContinue',isContinue)
    env = Env(slip=args.slip, one_det=args.one_det).get_env() 
    agent = Agent(env=env, 
                  model_path=PATH, 
                  logs_path=logs_path, 
                  debug=False, 
                  namespace='pg', 
                  gd=args.greedy_denom, 
                  learnrate=args.learn_rate,
                  det_action=args.det_action,
                  activation=args.activation,
                  need_greedy=args.need_greedy)
    if not isContinue:
        agent.init_session()
    else:
        agent.load()
    aug_rew = AugmentReward(selector=args.reward_type, rew_scale=args.rew_scale)
    rollout = PGRollOut(maxstep=500, 
                        clear=args.play_clear,
                        render=args.play_render,
                        sleep=args.play_sleep,
                        aug_rew=aug_rew,
                        agent=agent,
                        env=env)
    player = Player(env=env, 
                    agent=agent,
                    gamma=args.gamma,
                    render=args.play_render,
                    rollout=rollout)
    if args.type == 'train':
        player.train(epoch=args.train_epoch)
    elif args.type == 'play':
        player.agent.set_need_greedy(need_greedy=False)
        player.play(need_load=args.play_load, 
                    round=args.play_round)
    else:
        print('No options')
    env.close()        

