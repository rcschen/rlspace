import __init__
import numpy as np
import pickle, os, time
from utils import check_path
import tensorflow as tf
import argparse, gym
import numpy as np

LEARNING_RATE = 0.81
EPSILON=0.0
EPSILON=0.4
GAMMA = 0.96
rew_time=20

hist = {'G':0, 'H':0}
logs_path = os.path.join('./qnnv1log',str(int(time.time())))

def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    init_np[np.arange(len(data)), data] = 1
    return init_np

class Agent(object):
    def __init__(self, env, model_path='model', cont=True, ep=0):
        self.model_path = model_path
        self.env = env
        self.obses = None
        self.obses2 = None
        self.acts = None
        self.rews = None
        self.out_prob = None
        self.n_out = None
        self.optimize = None
        self.sess = None
        self.saver = None
        self.loss = None
        self.ep = ep
        print('ep ======',ep)
        if cont:
           self.load()

    def init_session(self):
        self.net()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)

    def close_session(self):
        if self.sess is not None:
            self.sess.close()

    def save(self):
        self.saver = tf.train.Saver()
        self.saver.save(self.sess, self.model_path)   

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.n_out = tf.get_default_graph().get_tensor_by_name('ndense2/BiasAdd:0')
        self.out_prob = tf.get_default_graph().get_tensor_by_name('out_prob:0')
        self.obses = tf.get_default_graph().get_tensor_by_name('obses:0')

    def net(self):
        self.obses = tf.placeholder(tf.float32, shape=[1,self.env.observation_space.n], name='obses')
        self.rew = tf.placeholder(tf.float32, shape=[1,], name='rew')

        self.target = tf.placeholder(tf.float32, shape=[1,self.env.action_space.n], name='obses')
        self.n_out = tf.layers.dense(self.obses, units=36, activation=tf.nn.relu, name='ndense1')         
        self.n_out = tf.layers.dense(self.n_out, units=self.env.action_space.n, name='ndense2')        
        print(self.n_out) 
        self.out_prob = tf.argmax(self.n_out, 1, name='out_prob')
        self.loss = tf.reduce_sum(tf.square(self.target - self.n_out))
        optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
        self.optimize = optimizer.minimize(self.loss)
        tf.summary.scalar('loss', self.loss)
        tf.summary.scalar('rew', tf.gather_nd(self.rew,[0]))

        self.summary = tf.summary.merge_all()
        self.writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())

    def get_acts(self, obses):
        obses = np_one_hot([obses], 16)
        if self.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.obses: obses}
        if np.random.uniform(0,1) < self.ep:
            action = self.env.action_space.sample()
        else:
            action = self.sess.run(self.out_prob, feed_dict = feed_dict)[0]
        return action

    def get_q(self, obses):
        obses = np_one_hot([obses], 16)
        if self.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.obses: obses}
        q = self.sess.run(self.n_out, feed_dict = feed_dict)
        return q

    def train(self, obses, target, rew, times): 
        feed_dict = {
            self.obses : np_one_hot([obses], 16),
            self.target: target,
            self.rew: [rew]
        }
        opt, loss, rew,sm = self.sess.run([self.optimize, self.loss, self.rew, self.summary], feed_dict=feed_dict)
        print('loss:{}'.format(loss))
        self.writer.add_summary(sm, times)


def rollout(env, agent=None, clear=False, render = False):
    obs, rew, done, info = env.reset(), 0 , False, {}
    obses, acts, rews = [], [], []
    idx = 0
    if render:
        env.render()
        os.system('clear') 
    while not done:
        if agent is None:
            obs_, rew, done, info = env.step(env.action_space.sample())
        else:
            act = np.squeeze(agent.get_acts(obs), axis=0)
#            act_space = np.arange(env.action_space.n)
#            act = np.random.choice(act_space, p=prob)
            obs_, rew, done, info = env.step(act)
            acts.append(act)
        if done :
            if rew == 1 or obs_==15:
                rew = rew+20
            else:
                rew = rew-10
        else:
            if obs == obs_:
               rew = rew-5
            else:
               rew = rew+10
        obses.append(obs)
        rews.append(rew)
        print('{}--->'.format(idx))
        if render:
            env.render()
            if clear:
                if done:
                   if rew <= 0:
                      hist['H'] += 1
                      print('T____________T')
                   else:
                      hist['G'] += 1
                      print('>____________^')
#                time.sleep(0.8)
                os.system('clear') 
        idx += 1
        obs = obs_
    return obses, acts, rews

class Player(object):
    def __init__(self,env, agent=None):
        self.env = env
        self.agent = agent

    def play(self):
        self.agent.load()
        print('start to play')
        for i in range(100):
            rollout(self.env, self.agent, True, True)
        print('success:{} , fail: {}'.format(hist['G'], hist['H']))

        self.env.close()

    def train(self, epoch=10000):
        self.agent.init_session()
        for i in range(epoch):
            s = self.env.reset()
            done = False
            while not done:      
                a = self.agent.get_acts(s)
                q = self.agent.get_q(s)
                s_, rew, done, info = self.env.step(a)
                if done :
                   if rew == 1 or s_==15:
                       rew = rew+20
                   else:
                       rew = rew-10
                else:
                   if s == s_:
                      rew = rew-5
                   else:
                      rew = rew+10
                a_ = self.agent.get_acts(s_)
                q_ = self.agent.get_q(s_)
                q[0, a] = rew + GAMMA*np.max(q_)
                self.agent.train(s, q, rew, i)
                s  = s_
            if i % 500 == 0:
                print('------->',i)
                self.agent.save()
        self.env.close() 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t')

 
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    print(isContinue)
    ep = 0
    if args.td == 'train':
        ep = 0.4
    env = gym.make('FrozenLake-v0')
    agent = Agent(env, 'frozenlakeqnn/qnn', isContinue, ep)
    player = Player(env, agent)
    getattr(player, args.td)()
    env.close()
