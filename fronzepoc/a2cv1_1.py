import __init__
import numpy as np
import pickle, os, time
from utils import check_path
import tensorflow as tf
import argparse, gym
import numpy as np
import random
from gym import wrappers
from gym import register
from fl88env import FLEnv
from q_nnv6_3 import Agent as QAgent 
from pgv2_2 import Agent as PGAgent
from q_nnv6_3 import Player, np_one_hot
from cp_common import RollOut

LEARNING_RATE = 0.001
GAMMA = 0.9
INITIAL_EPSILON = 0.5 # starting value of epsilon
FINAL_EPSILON = 0.01 # final value of epsilon
DT_LEN = 1000
BATCH_SIZE = 32

logs_path = os.path.join('__d_logs',__file__.split('.')[0], str(int(time.time())))
ACTPATH = check_path( os.path.join('__d_frozenlake',__file__.split('.')[0], 'act'))
CRTPATH = check_path( os.path.join('__d_frozenlake',__file__.split('.')[0], 'crt'))

class Actor(PGAgent):
    def __init__(self, env, model_path='model', cont=False, ep=INITIAL_EPSILON, debug=False):
        super(Actor, self).__init__(env, model_path, cont, ep, debug)

class Critic(QAgent):
    def __init__(self, env, model_path='model', cont=True, ep=INITIAL_EPSILON):
        super(Critic, self).__init__(env, model_path, cont, ep)

    def train(self, obses, actions, obses_, rewards, dones): 
        feed_dict = {
            self.obses : np_one_hot(obses, 16),
            self.target: np.array(self.get_target(obses_, rewards, dones)),
            self.reward: np.array(rewards),
            self.action: np.array(actions)
        }
        opt, loss, diffQ = self.sess.run([self.optimize, self.loss, self.diffQ], feed_dict=feed_dict)
        return diffQ
   
class Player:
    def __init__(self, env, isContinue=True, ep=INITIAL_EPSILON):
        self.env = env
        self.actor = Actor(env, ACTPATH, isContinue, ep)
        self.critic = Critic(env, CRTPATH, isContinue, ep)
        self.actor.init_session()
        self.critic.init_session()
        self.dataset = []
        self.rollout = RollOut()

    def play(self, need_load = True, round=100):
        if need_load:
            self.agent.load()
        self.rollout.sleep(0)
        self.rollout.clear(False)
        self.rollout.render(True)
        print('start to play')
        self.rollout.summary_reset() 
        for i in range(round):
            self.rollout.go(self.env, self.actor, need_hist=True)
        self.env.close()

    def renew_dataset(self, dt_tmp):
        self.dataset.extend(dt_tmp)
        if len(self.dataset) > DT_LEN:
            self.dataset = self.dataset[len(self.dataset)-DT_LEN:]
 
    def train(self, epoch=20000):
        for i in range(epoch):
            s = self.env.reset()
            done = False
            step = 0
            round_dataset = []
            while not done:      
                dataset_tmp = []
                a = self.actor.get_greedy_action([s])
                s_, rew, done, info = self.env.step(a)
                dataset_tmp.append((s, a, s_, rew, done))
                round_dataset.append((s, a, s_, rew, done))

                s  = s_
                self.renew_dataset(dataset_tmp)         
                if len(self.dataset) > BATCH_SIZE: 
                    ds = random.sample(self.dataset, BATCH_SIZE) 
                else:
                    ds = self.dataset
#                t = time.time() 
                diffQ = self.critic.train([dt[0] for dt in ds],
                             [dt[1] for dt in ds],
                             [dt[2] for dt in ds],
                             [dt[3] for dt in ds],
                             [dt[4] for dt in ds])
                self.actor.train([dt[0] for dt in ds],
                             [dt[1] for dt in ds],
                             diffQ,
                             i)
#                print(time.time()-t)
            if i>300 and i % 100 == 0:
                self.play(False)
                self.actor.save()
                self.critic.save()
        self.env.close() 


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t')

    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    print(isContinue)
    ep = 0
    register(
            id='FrozenLakeNotSlippery-v0',
            entry_point='gym.envs.toy_text:FrozenLakeEnv',
            kwargs={'map_name' : '4x4', 'is_slippery': False},
            )
#    env_name  = 'FrozenLake8x8-v0'
    env_name  = 'FrozenLake-v0'
#    env_name = 'FrozenLakeNotSlippery-v0'

    env = gym.make(env_name)   
    fe = FLEnv()
    env = fe.get_env()

    player = Player(env, isContinue)
    getattr(player, args.td)()
    if args.td == 'train':
        player.play()
    env.close()         
