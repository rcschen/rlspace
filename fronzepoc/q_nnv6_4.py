'''
ython q_nnv6_4.py -t train -c f --greedy_denom 10000 --learn_rate 0.001 --need_greedy true --play_load true --play_render true --play_round 1000 --play_clear "" --play_sleep 0 --slip true --one_det "" --gamma 0.96 --train_epoch 10000 --reward_type 0 --det_action true --activation no_act --rew_scale 100 --data_len 1000 --batch_size 32
'''
import __init__
import numpy as np
import os, time, argparse, random
from utils import check_path
from rollout import QRollOut, AugmentReward
from cp_common import np_one_hot, Env
from agent_base import QAgent
from player_base import QPlayerBase

logs_path = os.path.join('__d_logs',__file__.split('.')[0], str(int(time.time())))
PATH = check_path( os.path.join('__d_model',__file__.split('.')[0]))
PATH = os.path.join( PATH, 'model')


class Agent(QAgent):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, gamma):
        super(Agent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy,
                                    gamma=gamma)

    def get_action(self, obses):
        if self.net.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.net.obses: np_one_hot(obses, self.env.observation_space.n)}
        prob, nout = self.sess.run([self.net.out_prob, self.net.n_out], feed_dict = feed_dict)
        if self.det_action:
            act = np.argmax(prob.ravel())
        else:
            act = np.random.choice(self.env.action_space.n, p=np.squeeze(prob, axis=0))
        return act

    def train(self, obses, actions, obses_, rewards, dones): 
        feed_dict = {
            self.net.obses : np_one_hot(obses, self.env.observation_space.n),
            self.net.target: np.array(self.get_target(obses_, rewards, dones)),
            self.net.rews: np.array(rewards),
            self.net.acts: np.array(actions),
        }
        opt, loss, out, sm= self.sess.run([self.net.optimize, self.net.loss, self.net.n_out, self.sm_pg], feed_dict=feed_dict)
        self.sm.add_summary(sm, self.loop)
        self.loop+=1

    def get_q(self, obses):
        if self.net.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.net.obses: np_one_hot(obses, self.env.observation_space.n),
                    }
        q = self.sess.run(self.net.n_out, feed_dict = feed_dict)
        return q

    def get_target(self, obses_, rews, dones):
        target = []
        q_ = self.get_q(obses_)
        for i, d in enumerate(dones):
            if d:
                target.append(rews[i])           
            else:
                target.append(rews[i]+self.gamma*np.max(q_[i]))
        return target


class Player(QPlayerBase):
    def __init__(self,env, agent, gamma, render, rollout, data_len, batch_size):
        super(Player, self).__init__(env, agent, gamma, render, rollout, data_len, batch_size)

    def train(self, epoch=10000):
        for i in range(epoch):
            obs = self.env.reset()
            done = False
            round_dataset = []
            while not done:      
                dataset_tmp = []
                obs, act, obs_, rew, done = self.rollout.sample(obs)
                dataset_tmp.append((obs, act, obs_, rew, done))
                round_dataset.append((obs, act, obs_, rew, done))
                obs = obs_
                self.renew_dataset(dataset_tmp)         
                if len(self.dataset) > self.batch_size: 
                    ds = random.sample(self.dataset, self.batch_size) 
                else:
                    ds = self.dataset
                self.agent.train([dt[0] for dt in ds],
                             [dt[1] for dt in ds],
                             [dt[2] for dt in ds],
                             [dt[3] for dt in ds],
                             [dt[4] for dt in ds])
                '''
                self.agent.train([dt[0] for dt in self.dataset],
                                 [dt[1] for dt in self.dataset],
                                 [dt[2] for dt in self.dataset],
                                 [dt[3] for dt in self.dataset],
                                 [dt[4] for dt in self.dataset])
                '''
            print([dt[1] for dt in round_dataset])

            print('------{}---------> {}'.format(i, self.agent.ep))
#            print('aa:{}'.format([dt[1] for dt in self.dataset]))
            if i>300 and i % 100 == 0:
                print('------{}---------> {}'.format(i, self.agent.ep))
                self.play(need_load=False, round=100)
#                if self.rollout.get_result() >= 0.7:
                self.agent.save()
        self.env.close() 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--type",type=str, help="train or play", default='play')
    parser.add_argument("-c", "--cont", type=str, help="continue exist table", default='t')
    parser.add_argument("-e", "--greedy_denom", type=int, help="greedy_denom", default=10000)
    parser.add_argument("-l", "--learn_rate", type=float, help="learning rate", default=0.0001)
    parser.add_argument("-g", "--need_greedy", type=bool, help="need greedy", default=False)
    parser.add_argument("-i", "--play_load", type=bool, help="player need load", default=True)
    parser.add_argument("--play_render", type=bool, help="player rebder", default=False)
    parser.add_argument("--play_round", type=int, help="player round", default=1000)
    parser.add_argument("--play_clear", type=bool, help="player clear", default=False)
    parser.add_argument("--play_sleep", type=int, help="player sleep", default=0)
    parser.add_argument("--slip", type=bool, help="slipping", default=True)
    parser.add_argument("--one_det", type=bool, help="one deterministic", default=False)
    parser.add_argument("--gamma", type=float, help="gamma", default=0.99)
    parser.add_argument("--train_epoch", type=int, help="train epoch", default=10000)
    parser.add_argument("--reward_type", type=int, help="reward type", default=0)
    parser.add_argument("--det_action", type=bool, help="deterministic action", default=True)
    parser.add_argument("--activation", type=str, help="activation function", default='relu')
    parser.add_argument("--rew_scale", type=int, help="rew_scale", default=1)
    parser.add_argument("--data_len", type=int, help="data length", default=1000)
    parser.add_argument("--batch_size", type=int, help="batch size", default=50)

    args = parser.parse_args()
    print(args)
    isContinue = (args.cont in ['t','true','True', '1'])
    print('=================================================')
    print(args)
    print('=================================================')
    print('isContinue',isContinue)
    env = Env(slip=args.slip, one_det=args.one_det).get_env() 
    agent = Agent(env=env, 
                  model_path=PATH, 
                  logs_path=logs_path, 
                  debug=False, 
                  namespace='qnn', 
                  gd=args.greedy_denom, 
                  learnrate=args.learn_rate,
                  det_action=args.det_action,
                  activation=args.activation,
                  need_greedy=args.need_greedy,
                  gamma=args.gamma)
    if not isContinue:
        agent.init_session()
    else:
        agent.load()
    aug_rew = AugmentReward(selector=args.reward_type, rew_scale=args.rew_scale)
    rollout = QRollOut(maxstep=500, 
                        clear=args.play_clear,
                        render=args.play_render,
                        sleep=args.play_sleep,
                        aug_rew=aug_rew,
                        agent=agent,
                        env=env)
    player = Player(env=env, 
                    agent=agent,
                    gamma=args.gamma,
                    render=args.play_render,
                    rollout=rollout,
                    data_len=args.data_len, 
                    batch_size=args.batch_size)
    if args.type == 'train':
        player.train(epoch=args.train_epoch)
    elif args.type == 'play':
        player.agent.set_need_greedy(need_greedy=False)
        player.play(need_load=args.play_load, 
                    round=args.play_round)
    else:
        print('No options')
    env.close()        

