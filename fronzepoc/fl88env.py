import gym
from gym import wrappers
from gym import register

class FLEnv:
    def __init__(self, env_name = 'FrozenLake-v0'):
        self.env = gym.make(env_name)
        self.env = self.env.unwrapped

    def set_probability(self):
        self.env.P[0][1] = [(1.0, 4, 0.0, False)]
#        print(self.env.P[0])
        self.env.P[4][1] = [(1.0, 8, 0.0, False)]
#        print(self.env.P[4])
        self.env.P[8][2] = [(1.0, 9, 0.0, False)]
#        print(self.env.P[8])
        self.env.P[9][1] = [(1.0, 13, 0.0, False)]
#        print(self.env.P[9])
        self.env.P[13][2] = [(1.0, 14, 0.0, False)]
#        print(self.env.P[13])
        self.env.P[14][2] = [(1.0, 15, 1.0, True)]
#        print(self.env.P[15])

    def get_env(self, modify=True):
        if modify:
            self.set_probability()
        return self.env

if __name__ == '__main__':
    fe = FLEnv()
    env = fe.get_env()
    print(env.observation_space.n)
