import numpy as np
import tensorflow as tf

class Model:
    def __init__(self):
        def conv_layer(x, conv, stride = 1):
            return tf.nn.conv2d(x, conv, [1, stride, stride, 1], padding = 'SAME')
        def pooling(x, k = 2, stride = 2):
            return tf.nn.max_pool(x, ksize = [1, k, k, 1], strides = [1, stride, stride, 1], padding = 'SAME')
        self.X = tf.placeholder(tf.float32, [None, 80, 80, 4])
        self.Y = tf.placeholder(tf.float32, [None, 2])
        self.w_conv1 = tf.Variable(tf.truncated_normal([8, 8, 4, 32], stddev = 0.1))
        self.b_conv1 = tf.Variable(tf.truncated_normal([32], stddev = 0.01))
        conv1 = tf.nn.relu(conv_layer(self.X, self.w_conv1, stride = 4) + self.b_conv1)
        pooling1 = pooling(conv1)
        self.w_conv2 = tf.Variable(tf.truncated_normal([4, 4, 32, 64], stddev = 0.1))
        self.b_conv2 = tf.Variable(tf.truncated_normal([64], stddev = 0.01))
        conv2 = tf.nn.relu(conv_layer(pooling1, self.w_conv2, stride = 2) + self.b_conv2)
        self.w_conv3 = tf.Variable(tf.truncated_normal([3, 3, 64, 64], stddev = 0.1))
        self.b_conv3 = tf.Variable(tf.truncated_normal([64], stddev = 0.01))
        conv3 = tf.nn.relu(conv_layer(conv2, self.w_conv3) + self.b_conv3)
        pulling_size = int(conv3.shape[1]) * int(conv3.shape[2]) * int(conv3.shape[3])
        conv3 = tf.reshape(conv3, [-1, pulling_size])
        self.w_fc1 = tf.Variable(tf.truncated_normal([pulling_size, 256], stddev = 0.1))
        self.b_fc1 = tf.Variable(tf.truncated_normal([256], stddev = 0.01))
        self.w_fc2 = tf.Variable(tf.truncated_normal([256, 2], stddev = 0.1))
        self.b_fc2 = tf.Variable(tf.truncated_normal([2], stddev = 0.01))
        fc_1 = tf.nn.relu(tf.matmul(conv3, self.w_fc1) + self.b_fc1)
        self.logits = tf.matmul(fc_1, self.w_fc2)  + self.b_fc2
        self.cost = tf.reduce_sum(tf.square(self.Y - self.logits))
        self.optimizer = tf.train.AdamOptimizer(learning_rate = 0.1).minimize(self.cost)
        
X = np.random.normal(size=(100, 10))
Y = np.random.randint(0, 2, size=(100,1))
epoch = 10

sess = tf.InteractiveSession()
first_model = Model()
second_model = Model()
sess.run(tf.global_variables_initializer())
tf.trainable_variables()
        
trainable = tf.trainable_variables()
print(trainable)
for i in range(len(trainable)//2):
    print(i+len(trainable)//2)
    assign_op = trainable[i+len(trainable)//2].assign(trainable[i])
    print(sess.run(assign_op))
