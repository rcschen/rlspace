import __init__
import numpy as np
import gym, time, os, argparse
from gym import wrappers
from gym import register
import random as pr
from utils import save, load, evaluate_policy, check_path
from cp_common import get_action


def extract_policy(q, gamma = 1.0):
    """ Extract the policy given a value-function """
    policy = np.zeros(env.nS)
    for s in range(env.nS):
        policy[s] = np.argmax(q[s])
    return policy


EPISODES = 10000
ACT_TYPE = 1
DISCOUNT = 0.96
ALPHA = 0.81
INITIAL_EPSILON = 0.5 # starting value of epsilon
FINAL_EPSILON = 0.01 # final value of epsilon

def get_qtable(env): 
    qtable = np.zeros((env.observation_space.n, env.action_space.n))
    rmth = 0.3
    for i in range(EPISODES):
        s = env.reset()
        complete = False
        history = []
        G = 0
        rmth -= (INITIAL_EPSILON - FINAL_EPSILON)/10000
        while complete == False:
            a = get_action(qtable, s, ACT_TYPE, rm_th=rmth)

            s_, reward, complete, _ = env.step(a)
            a_ = np.argmax(qtable[s_])
            if False:
                td_target = reward
            else:
                td_target = reward + DISCOUNT * qtable[s_, a_] 
            td_delta = td_target - qtable[s, a]
            qtable[s,a] += ALPHA*td_delta
            print('rmth: {}'.format(rmth))
            print('qtable:{}'.format(qtable))
            print('{} ==> action:{}, reward:{} ,s_:{}'.format(i,a, reward, s_))
            s = s_
    return qtable
            

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t') 
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    PATH = check_path( os.path.join('__d_frozenlake',__file__.split('.')[0]))
    PATH = os.path.join(PATH, 'qtable')

    register(
            id='FrozenLakeNotSlippery-v0',
            entry_point='gym.envs.toy_text:FrozenLakeEnv',
            kwargs={'map_name' : '4x4', 'is_slippery': False},
            )
    #env_name  = 'FrozenLake8x8-v0'
    env_name  = 'FrozenLake-v0'
#    env_name = 'FrozenLakeNotSlippery-v0'
    gamma = 1.0
    env = gym.make(env_name)
    env = env.unwrapped
    if args.td == 'train':
        q = get_qtable(env)
        print('q:{}'.format(q))
        policy = extract_policy(q)
        save(policy, PATH)
    policy = load(PATH)
    policy_score = evaluate_policy(env, policy, gamma, n=1000)
    print('Policy average score = ', policy_score)
