import gym, argparse
import os, time
import tensorflow as tf
LEARNING_RATE=1e-3
import numpy as np

hist = {'G':0, 'H':0}

def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    init_np[np.arange(len(data)), data] = 1
    return init_np

class Agent(object):
    def __init__(self, env, model_path='model'):
        self.env = env
        self.obses = None
        self.acts = None
        self.rews = None
        self.out_prob = []
        self.optimize = None
        self.sess = None
        self.saver = None
        self.model_path = model_path
        self.loss = None

    def init_session(self):
        self.net()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)

    def close_session(self):
        if self.sess is not None:
            self.sess.close()

    def save(self):
        self.saver = tf.train.Saver()
        self.saver.save(self.sess, self.model_path)   

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.out_prob = tf.get_default_graph().get_tensor_by_name('out_prob:0')
        self.obses = tf.get_default_graph().get_tensor_by_name('obses:0')

    def net(self):
        self.obses = tf.placeholder(tf.float32, shape=[None, self.env.observation_space.n],name='obses')
        self.acts = tf.placeholder(tf.int32, name='acts')
        self.rews = tf.placeholder(tf.float32, name='rews')
        n_out = tf.layers.dense(self.obses, units=36, activation=tf.nn.relu, name='dense1')         
        n_out = tf.layers.dense(n_out, units=self.env.action_space.n, name='dense2')         
        self.out_prob = tf.nn.softmax(n_out, name='out_prob')
        n_out = tf.reduce_sum(-tf.log(self.out_prob)*tf.one_hot(self.acts, self.env.action_space.n), axis=1) 
        self.loss = tf.reduce_mean(n_out * self.rews)
        optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
        self.optimize = optimizer.minimize(self.loss)

    def get_acts(self, obses):
        if self.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.obses: np_one_hot(obses, self.env.observation_space.n)}
        return self.sess.run(self.out_prob, feed_dict = feed_dict)

    def train(self, obses, acts, rews):
        feed_dict = {
            self.obses : np_one_hot(obses, self.env.observation_space.n),
            self.acts: acts,
            self.rews: rews
        }
        opt, loss = self.sess.run([self.optimize, self.loss], feed_dict=feed_dict)
        print('loss:{}'.format(loss))



def rollout(env, agent=None, clear=False, render = False):
    obs, rew, done, info = env.reset(), 0 , False, {}
    obses, acts, rews = [], [], []
    idx = 0
    if render:
        env.render()
        os.system('clear') 
    while not done:
        if agent is None:
            obs_, rew, done, info = env.step(env.action_space.sample())
        else:
            prob = np.squeeze(agent.get_acts([obs]), axis=0)
            act_space = np.arange(env.action_space.n)
            act = np.random.choice(act_space, p=prob)
            obs_, rew, done, info = env.step(act)
            acts.append(act)
        if done :
            if rew == 1 or obs_==15:
                rew = rew+20
            else:
                rew = rew-10
        else:
            if obs == obs_:
               rew = rew-5
            else:
               rew = rew+10
        obses.append(obs)
        rews.append(rew)
        print('{}--->'.format(idx))
        if render:
            env.render()
            if clear:
                if done:
                   if rew <= 0:
                      hist['H'] += 1
                      print('T____________T')
                   else:
                      hist['G'] += 1
                      print('>____________^')
#                time.sleep(0.8)
                os.system('clear') 
        idx += 1
        obs = obs_
    return obses, acts, rews

class Player(object):
    def __init__(self,env, agent=None):
        self.env = env
        self.agent = agent

    def play(self):
        self.agent.load()
        print('start to play')
        for i in range(100):
            rollout(self.env, self.agent, True, True)
            print('############################', i)
        print('success:{} , fail: {}'.format(hist['G'], hist['H']))
        self.env.close()

    def train(self, epoch=10000):
        print('start to train')
        self.agent.init_session()
        for i in range(epoch):
            print('=================== train round:',i)
            obses, acts, rews = rollout(self.env, self.agent)
            self.agent.train(obses, acts, rews)
            if i % 50 == 0:
                self.agent.save()
        self.agent.close_session()
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    args = parser.parse_args()
    print(args.td)

    env = gym.make('FrozenLake-v0')
    agent = Agent(env, 'frozenlake_pg/pg')
    player = Player(env, agent)
    getattr(player, args.td)()
    env.close()         
