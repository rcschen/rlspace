import __init__
import gym, argparse
import os, time
import tensorflow as tf
import numpy as np
from utils import get_summary 
from gym import wrappers
from gym import register

from fl88env import FLEnv
from tensorflow.python import debug as tf_debug

LEARNING_RATE=0.001

INITIAL_EPSILON = 0.5 # starting value of epsilon
FINAL_EPSILON = 0.01 # final value of epsilon

hist = {'G':0, 'H':0}
logs_path = os.path.join('./pglog',str(int(time.time())))

def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    init_np[np.arange(len(data)), data] = 1
    return init_np

class Agent(object):
    def __init__(self, env, model_path='model',cont=False, ep=INITIAL_EPSILON, debug=False, namespace='pg'):
        self.env = env
        self.obses = None
        self.acts = None
        self.rews = None
        self.out_prob = []
        self.optimize = None
        self.sess = None
        self.saver = None
        self.model_path = model_path
        self.loss = None
        self.ep = ep
        self.debug = debug
        self.namespace = namespace
        if cont:
           self.load()

    def init_session(self):
        self.net()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)
        if self.debug:
            self.sess = tf_debug.TensorBoardDebugWrapperSession(self.sess, "localhost:8887")

    def close_session(self):
        if self.sess is not None:
            self.sess.close()

    def save(self):
        self.saver = tf.train.Saver()
        self.saver.save(self.sess, self.model_path)   

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.out_prob = tf.get_default_graph().get_tensor_by_name('{}/out_prob:0'.format(self.namespace))
        self.obses = tf.get_default_graph().get_tensor_by_name('pg_obses:0')

    def setPlaceholder(self):
        self.obses = tf.placeholder(tf.float32, shape=[None, self.env.observation_space.n],name='pg_obses')
        self.acts = tf.placeholder(tf.int32, name='acts')
        self.rews = tf.placeholder(tf.float32, name='rews')

    def net(self):
        self.setPlaceholder()
        with tf.variable_scope(self.namespace):
            self.n_out = tf.layers.dense(self.obses, 
                                units=64, 
                                activation=tf.nn.relu, 
#                                activation=tf.nn.tanh,  # tanh activation
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                name='dense1')         
            self.n_out = tf.layers.dense(self.n_out, 
#                                activation=tf.nn.tanh,  # tanh activation
                                activation=tf.nn.relu, 

                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                units=self.env.action_space.n, 
                                name='dense2') 

            tf.summary.histogram('n_out', self.n_out)
  
            self.out_prob = tf.nn.softmax(self.n_out, name='out_prob')
        tf.summary.histogram('out_prob', self.out_prob)

        self.optimize_section_CEL()
        self.getSummary()

    def optimize_section(self):
        self.out = - tf.reduce_sum(tf.log(self.n_out)*tf.one_hot(self.acts, self.env.action_space.n), axis=1)
        tf.summary.scalar('rews', self.rews)

        self.loss = tf.reduce_mean(self.out * self.rews)
        optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
        self.optimize = optimizer.minimize(self.loss)

    def getSummary(self):
        tf.summary.scalar('loss', self.loss)
        self.summary = tf.summary.merge_all()
        self.writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())

    def optimize_section_CEL(self):
        # to maximize total reward (log_p * R) is to minimize -(log_p * R), and the tf only have minimize(loss)
        print(tf.cast(tf.one_hot(self.acts, self.env.action_space.n),tf.int64))
        self.out = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.n_out, 
                                                                  labels=self.acts)   # this is negative log of chosen action
        # or in this way:
        # neg_log_prob = tf.reduce_sum(-tf.log(self.all_act_prob)*tf.one_hot(self.tf_acts, self.n_actions), axis=1)
        self.loss = tf.reduce_mean(self.out * self.rews)  # reward guided loss
        optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
        self.optimize = optimizer.minimize(self.loss)

    def get_acts(self, obses, need_greedy=False):
        if need_greedy:
            return self.get_greedy_action(obses)
        else:
            return self.get_action(obses)

    def get_greedy_action(self, obses):
        if np.random.uniform(0,1) <= self.ep:
            action = self.env.action_space.sample()
        else:
            action = self.get_acts(obses)
        self.ep -= (INITIAL_EPSILON - FINAL_EPSILON)/100000 
        return action

    def get_action(self, obses):
        if self.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.obses: np_one_hot(obses, self.env.observation_space.n)}
        return np.argmax(self.sess.run(self.out_prob, feed_dict = feed_dict)[0])

    def train(self, obses, acts, rews, times):
        feed_dict = {
            self.obses : np_one_hot(obses, self.env.observation_space.n),
            self.acts: acts,
            self.rews: rews
        }
        opt, loss,sm, out = self.sess.run([self.optimize, self.loss, self.summary, self.out], feed_dict=feed_dict)
        print('out:{}'.format(out))
        print('loss:{}'.format(loss))
        self.writer.add_summary(sm, times)



def rollout(env, agent=None, need_greedy = False, clear=False, render = False, sleep = 0):
    obs, rew, done, info = env.reset(), 0 , False, {}
    obses, acts, rews = [], [], []
    idx = 0
    can_train=False
    if render:
        env.render()
    while not done:
        if agent is None:
            obs_, rew, done, info = env.step(env.action_space.sample())
        else:
            act = agent.get_acts([obs], need_greedy = need_greedy)
            obs_, rew, done, info = env.step(act)
            acts.append(act)
        if done :
            if rew == 1:
                rew = rew+10
            elif rew==0 and obs_== 15:
                 rew = rew+10

        '''
        if done :
            if rew == 1:
                rew = rew+10
            elif rew==0 and obs_== 15:
                 rew = rew+10
            else:
                 rew = rew-10
#        else:
#            rew = rew+2
        '''
        obses.append(obs)
        rews.append(rew)
        print(get_summary(env, done, rew, render, clear, sleep))
        idx += 1
        obs = obs_
    print('act >>>>>',acts)

    return obses, acts, rews, can_train

class Player(object):
    def __init__(self,env, agent=None):
        self.env = env
        self.agent = agent

    def play(self):
        self.agent.load()
        print('start to play')
        for i in range(100):
            rollout(env = self.env, 
                    agent = self.agent, 
                    need_greedy = False,
                    clear = True,
                    render = True,
                    sleep=0)
        self.env.close()

    def get_discount_sum_of_rewards1(self, rewards, gamma=0.7):
        print(rewards)
        adv = np.zeros_like(rewards)
        print(adv)
        c = 0
        for i, r in enumerate(rewards[::-1]):
            c = r + gamma *c
            adv[i] = c
        adv -= np.min(adv)
        adv /= (np.std(adv) + 1e-10)
        print('###################')
        print('####',adv[::-1])
        print('###################')

        return adv[::-1]

    def get_discount_sum_of_rewards(self, rewards, gamma=0.95):
        print(rewards)
        adv = np.zeros_like(rewards)
        print(adv)
        c = 0
        for r in rewards:
            c = gamma*c + r
        for i, r in enumerate(rewards[::-1]):
            adv[i] = c
        return adv[::-1]

    def train(self, epoch=20000):
        print('start to train')
        self.agent.init_session()
        i=0
        while i < epoch:
            obses, acts, rews, can_train = rollout(env = self.env, 
                                                   agent = self.agent, 
                                                   need_greedy = True,
                                                   clear = False,
                                                   render = False,
                                                   sleep=0)
            dis_rews = self.get_discount_sum_of_rewards(rews)
            self.agent.train(obses, acts, dis_rews, i)
            i+=1
            if i % 1000 == 0:
                self.agent.save()
        self.agent.close_session()
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t')
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    args = parser.parse_args()
    print(args.td)
    register(
            id='FrozenLakeNotSlippery-v0',
            entry_point='gym.envs.toy_text:FrozenLakeEnv',
            kwargs={'map_name' : '4x4', 'is_slippery': False},
            )
#    env_name  = 'FrozenLake8x8-v0'
    env_name  = 'FrozenLake-v0'
#    env_name = 'FrozenLakeNotSlippery-v0'

    env = gym.make(env_name)   
#    fe = FLEnv()
#    env = fe.get_env()

    agent = Agent(env, 'frozenlake_pg/pg', isContinue)
    player = Player(env, agent)
    getattr(player, args.td)()
    if args.td == 'train':
        player.play()
    env.close()         
