import __init__
from gym import wrappers 
from common.env_base import BaseEnv
from common import wrappers as wp
from common import atari_wrappers as awp
import gym 
def make_env(env):
    env = wp.MaxAndSkipEnv(env)
    env = wp.FireResetEnv(env)
    env = wp.ImageToPyTorch(env)
    env = wp.BufferWrapper(env, 4)
    return wp.ScaledFloatFrame(env)

def atari_make_env(env_id):
    """
    Create a wrapped atari Environment

    :param env_id: (str) the environment ID
    :return: (Gym Environment) the wrapped atari environment
    """
    env = gym.make(env_id)
    assert 'NoFrameskip' in env.spec.id
    env = awp.NoopResetEnv(env, noop_max=30)
    env = awp.MaxAndSkipEnv(env, skip=4)
    return env

class Env(BaseEnv):
    def __init__(self):
        super(Env, self).__init__('AirRaid-v0')
        self.env = atari_make_env('AirRaidNoFrameskip-v0')

    def get_env(self):
        self.env.observation_space.n=self.env.observation_space.shape[0]
        self.env = wrappers.Monitor(self.env, '__d_record', force=True) 
        return self.env

    def set_probability(self):
        pass

if __name__ == '__main__':
    env = Env().get_env()
    print(dir(env))
    print(env.observation_space.n)
    env.reset()
    env.step(1)
