import __init__, time
from utils import get_summary 
import numpy as np
from common_base import ResultSummaryBase, RollOutBase
import gym

def rollout(env, agent=None, clear=False, render=False, MAXSTEP=5000):
    obs, rew, done = env.reset(), 0, False
    obses, actses, rewses = [], [], []
    step = 0
    while not done:
        if agent is None:
            obs_, rew, done, info = env.step(env.action_space.sample())
        else:
            prob = agent.get_acts([obs])
#            act_space = np.arange(env.action_space.n)
#            act = np.random.choice(act_space, p=prob)
            obs_, rew, done, info = env.step(prob)
#            actses.append(act)
        obses.append(obs)
#        actses.append(act)
        rewses.append(rew)
        obs = obs_
        get_summary(env, False, rew, render, clear )
        step+=1
        if step > MAXSTEP:
           print('max step:{} is reached'.format(MAXSTEP))
           break
    print('------------------- len rews: {}'.format(sum(rewses)))

    return obses, actses, rewses


class Env(object):
    def __init__(self):
        self.env = None
        self.env_name  = 'CartPole-v1'
        self.env = gym.make(self.env_name) 
        self.env.observation_space.n=4

    def get_env(self):
        return self.env

