import __init__
import time
from common_base import ResultSummaryBase, RollOutBase
from common_base import AugmentReward as AugmentRewardBase

class ResultSummary(ResultSummaryBase):
    def __init__(self):
        super(ResultSummary, self).__init__()

    def cal_result(self):
        pass

    def reset_hist(self):
        self.hist = 0
 
    def set_summary(self, rews):
        self.hist = len(rews)


class PGRollOut(RollOutBase):
    def __init__(self, maxstep=700, clear=False, render=False, sleep =0, aug_rew=None, agent=None, env=None):
        super(PGRollOut, self).__init__(maxstep, clear, render, sleep, aug_rew, agent, env)
        self.summary = ResultSummary()

    def sample(self):
        obs, rew, done, info = self.env.reset(), 0 , False, {}
        obses, acts, rews = [], [], []
        while not done:
            act = self.agent.get_acts([obs])
            obs_, rew, done, info = self.env.step(act)
            if self.aug_rew:
                rew = self.aug_rew.get_reward(done=done, 
                                              rew=rew, 
                                              obs=obs, 
                                              obs_=obs_,
                                              rew_length=len(rews))
            acts.append(act)
            obses.append(obs)
            rews.append(rew)
            obs = obs_
        return obses, acts, rews

    def go(self, need_hist=False):
        obs, rew, done = self.env.reset(), 0, False
        obses, actses, rewses = [], [], []
        step=0
        while not done:
            act = self.agent.get_acts([obs])
            obs_, rew, done, info = self.env.step(act)
            if self._render:
                env.render()
            actses.append(act)
            obses.append(obs)
            rewses.append(rew)
            obs = obs_
            if done:
                self.summary.set_summary(rewses)
            if done and need_hist:
                print('length = {}'.format(self.summary.get_hist()))
            step+=1
        return obses, actses, rewses


class AugmentReward(AugmentRewardBase):
    def __init__(self, selector, rew_scale):
        super(AugmentReward, self).__init__(rew_scale)
        self.func = getattr(self,'ar_{}'.format(selector))

    def ar_0(self, **kwargs):
        return kwargs.get('rew')

    def ar_1(self, **kwargs):
        if self.done :
            if self.rew == 1:
                return 10
            else:
                return -10
        else:
            return self.rew

    def ar_2(self, **kwargs):
        if self.done :
            if self.rew == 1:
                return 20
            else:
                return -20
        else:
            return 1

    def ar_3(self, **kwargs):
        if self.done and self.rew == 1:
            return 10
        else: 
            return 0


