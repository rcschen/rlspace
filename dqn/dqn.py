'''
ython q_nnv6_4.py -t train -c f --greedy_denom 10000 --learn_rate 0.001 --need_greedy true --play_load true --play_render true --play_round 1000 --play_clear "" --play_sleep 0 --slip true --one_det "" --gamma 0.96 --train_epoch 10000 --reward_type 0 --det_action true --activation no_act --rew_scale 100 --data_len 1000 --batch_size 32
'''
import __init__
from common.arg_reward import AugmentReward
from .rollout import QRollOut
from .agent import QAgent as Agent
from .agent import PTQAgent 
from .player import QPlayerBase as Player
from .net import QNet, CNNQNet

class Executor(object):
    def __init__(self, env, args, model_path, log_path, round_mgt, summary=None, net=QNet):
        self.env = env
        self.args = args
        self.model_path = model_path
        self.log_path = log_path
        self.args.cont = (args.cont in ['t','true','True', '1'])
        self.agent = None
        self.arg_rew = None
        self.namespace='qnn'
        self.net = net(env=env, namespace=self.namespace, learnrate=args.learn_rate, activation=args.activation, optimizer=args.optimizer)
        self.round_mgt = round_mgt
        self.setAgent()
        self.setAugReward()
        self.setRollout(summary, args.maxtrystep)
        self.setPlayer()

    def setAgent(self):
        self.agent = Agent(env=self.env, 
                           model_path=self.model_path, 
                           logs_path=self.log_path, 
                           debug=False, 
                           namespace=self.namespace,
                           det_action=self.args.det_action,
                           gd=self.args.greedy_denom, 
                           need_greedy=self.args.need_greedy,
                           gamma=self.args.gamma,
                           net=self.net)
        if not self.args.cont:
            self.agent.init_session()
        else:
            self.agent.load()

    def setAugReward(self):
        self.aug_rew = AugmentReward(selector=self.args.reward_type, rew_scale=self.args.rew_scale)

    def setRollout(self, summary=None, maxstep=500):
        self.rollout = QRollOut(maxstep=maxstep, 
                                clear=self.args.play_clear,
                                render=self.args.play_render,
                                sleep=self.args.play_sleep,
                                aug_rew=self.aug_rew,
                                agent=self.agent,
                                env=self.env,
                                summary=summary)
    def setPlayer(self):
        self.player = Player(env=self.env, 
                        agent=self.agent,
                        gamma=self.args.gamma,
                        render=self.args.play_render,
                        rollout=self.rollout,
                        play_round=self.args.play_round,
                        data_len=self.args.data_len, 
                        batch_size=self.args.batch_size,
                        round_mgt = self.round_mgt)
    def train(self):
        self.player.train(epoch=self.args.train_epoch)
        self.env.close()

    def play(self):
        self.player.agent.set_need_greedy(need_greedy=False)
        self.player.play(need_load=self.args.play_load, 
                         round=self.args.play_round)
        self.env.close()        

class PTExecutor(Executor):
    def __init__(self, env, args, model_path, log_path, round_mgt, summary=None, net=QNet, tnet=QNet):
        self.tnamespace='tqnn'
        self.tnet = tnet(env=env, namespace=self.tnamespace, learnrate=args.learn_rate, activation=args.activation, optimizer=args.optimizer)
        super(PTExecutor, self).__init__(env=env,
                                         args=args,
                                         model_path=model_path,
                                         log_path=log_path,
                                         round_mgt=round_mgt,
                                         summary=summary,
                                         net=net)
    def setAgent(self):
        self.agent = PTQAgent(env=self.env, 
                           model_path=self.model_path, 
                           logs_path=self.log_path, 
                           debug=False, 
                           namespace=self.namespace,
                           det_action=self.args.det_action,
                           gd=self.args.greedy_denom, 
                           need_greedy=self.args.need_greedy,
                           gamma=self.args.gamma,
                           net=self.net,
                           tnet=self.tnet)
        if not self.args.cont:
            self.agent.init_session()
        else:
            self.agent.load()

