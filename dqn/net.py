import tensorflow as tf

from common.net_base import NetBase, CNNNetBase
from common.inputs import  get_ph_by_env


class QNet(NetBase):
    def __init__(self, env, namespace, learnrate, activation, optimizer):
        super(QNet, self).__init__(env, namespace, learnrate, activation, optimizer=optimizer)

    def load(self):
        super().load()
        self.obses = tf.get_default_graph().get_tensor_by_name('{}/obses:0'.format(self.namespace))
        self.acts = tf.get_default_graph().get_tensor_by_name('{}/acts:0'.format(self.namespace))
        self.rews = tf.get_default_graph().get_tensor_by_name('{}/rews:0'.format(self.namespace))
        self.n_out = tf.get_default_graph().get_tensor_by_name('{}/n_out:0'.format(self.namespace))
        self.out_prob = tf.get_default_graph().get_tensor_by_name('{}/out_prob:0'.format(self.namespace))
        self.loss = tf.get_default_graph().get_tensor_by_name('{}/loss:0'.format(self.namespace))
        self.optimize = tf.get_default_graph().get_operation_by_name('{}/optimize'.format(self.namespace))
        self.target = tf.get_default_graph().get_tensor_by_name('{}/targ:0'.format(self.namespace))
        self.diffQ = tf.get_default_graph().get_tensor_by_name('{}/diffq:0'.format(self.namespace))

    def set_placeholder(self):
        with tf.variable_scope(self.namespace):
            self.obses, self.obses_pro = get_ph_by_env(self.env.observation_space, name='obses')
            self.acts = tf.placeholder(tf.int32, shape=[None, ], name='acts')
            self.rews = tf.placeholder(tf.float32, shape=[None, ], name='rews')
            self.target = tf.placeholder(tf.float32, shape=[None,], name='targ')
            self.success = tf.placeholder(tf.float32, name='success')

    def set_optimize_(self):
        with tf.variable_scope('{}/'.format(self.namespace)):
            Q_action = tf.reduce_sum(tf.multiply(self.n_out, tf.one_hot(self.acts, depth=self.env.action_space.n, dtype=tf.float32)), reduction_indices = 1)
            self.diffQ = tf.identity(self.target - Q_action, name='diffq')
            self.loss = tf.reduce_mean(tf.square(self.diffQ), name='loss')
            self.optimize = self.optimizer(self.learnrate).minimize(self.loss, name='optimize')
            tf.summary.scalar("loss", self.loss)

    def set_optimize(self):
        with tf.variable_scope('{}/'.format(self.namespace)):
            Q_action = tf.reduce_sum(tf.multiply(self.n_out, tf.one_hot(self.acts, depth=self.env.action_space.n, dtype=tf.float32)), reduction_indices = 1)
            self.diffQ = tf.identity(self.target - Q_action, name='diffq')
            self.loss = tf.reduce_mean(tf.square(self.diffQ), name='loss')
            tf.summary.scalar("loss", self.loss)
            optimizer = self.optimizer(self.learnrate)
            t_params = self.get_trainable()
            grads_and_vars = optimizer.compute_gradients(self.loss, t_params)
            grads = [g for g, v in grads_and_vars]
            vs = [v for g, v in grads_and_vars]
#            grads, _ = tf.clip_by_global_norm(grads, 0.5)
            grads_and_vars = list(zip(grads, vs))
            print('gradsgradsgrads:', grads_and_vars)
            [tf.summary.histogram("%s-grad" % g[1].name, g[0]) for g in grads_and_vars]
            self.optimize = optimizer.apply_gradients(grads_and_vars)

class CNNQNet(QNet, CNNNetBase):
    def __init__(self, env, namespace, learnrate, activation, optimizer):
        QNet.__init__(self, env, namespace, learnrate, activation, optimizer)

