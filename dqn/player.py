import random
from common.player_base import PlayerBase
import numpy as np

class QPlayerBase(PlayerBase):
    def __init__(self,env, agent, gamma, render, rollout, play_round, data_len, batch_size, round_mgt):
        super(QPlayerBase, self).__init__(env, agent, gamma, render, rollout, play_round, round_mgt)
        self.dataset = []
        self.data_len = data_len
        self.batch_size = batch_size

    def renew_dataset(self, dt_tmp):
        self.dataset.extend(dt_tmp)
        if len(self.dataset) > self.data_len:
            self.dataset = self.dataset[len(self.dataset)-self.data_len:]

    def train(self, epoch=10000):
        print('train train train')
        for i in range(epoch):
            obs = self.env.reset()
            done = False
            round_dataset = []
            while not done:      
                dataset_tmp = []
                obs, act, obs_, rew, done = self.rollout.sample(obs)
                dataset_tmp.append((obs, act, obs_, rew, done))
                round_dataset.append((obs, act, obs_, rew, done))
                obs = obs_
                self.renew_dataset(dataset_tmp)         
                if len(self.dataset) > self.batch_size: 
                    ds = random.sample(self.dataset, self.batch_size) 
                else:
                    ds = self.dataset
                self.agent.train([dt[0] for dt in ds],
                             [dt[1] for dt in ds],
                             [dt[2] for dt in ds],
                             [dt[3] for dt in ds],
                             [dt[4] for dt in ds])
            print('------{}---------> {}'.format(i, self.agent.ep))
            self.round_mgt.calculate_best_reward([dt[3] for dt in round_dataset], i)
            if self.round_mgt.get_notification('save'):
                self.agent.save()
                print('save the model')
            if i>300 and i % 20 == 0:
                print('------{}---------> {}'.format(i, self.agent.ep))
                self.play(need_load=False, round=self.play_round)

