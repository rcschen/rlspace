import numpy as np
from .net import QNet
from common.agent_base import AgentBase
import tensorflow as tf

class QAgent(AgentBase):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, det_action, need_greedy, gamma, net):
        super(QAgent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace,
                                    det_action=det_action, 
                                    gd=gd, 
                                    need_greedy=need_greedy)
        self.gamma = gamma
        self.net = net

    def get_action(self, obses):
        prob, nout = self.get_pq(obses)
        if self.det_action:
            act = np.argmax(prob.ravel())
        else:
            act = np.random.choice(self.env.action_space.n, p=np.squeeze(prob, axis=0))
        return act

    def train(self, obses, actions, obses_, rewards, dones): 
        feed_dict = {
            self.net.obses: np.array(obses),
            self.net.target: np.array(self.get_target(obses_, rewards, dones)),
            self.net.rews: np.array(rewards),
            self.net.acts: np.array(actions),
        }
        opt, loss, out, sm, obsss= self.sess.run([self.net.optimize, 
                                                  self.net.loss, 
                                                  self.net.n_out, 
                                                  self.sm_pg, 
                                                  self.net.obses], feed_dict=feed_dict)
        self.sm.add_summary(sm, self.loop)
        self.loop+=1

    def get_pq(self, obses):
        if self.net.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = {self.net.obses: np.array(obses)}
        p, q = self.sess.run([self.net.out_prob, self.net.n_out], feed_dict=feed_dict)
        return p, q

    def get_target(self, obses_, rews, dones):
        target = []
        p, q_ = self.get_pq(obses_)
        for i, d in enumerate(dones):
            if d:
                target.append(rews[i])           
            else:
                target.append(rews[i]+self.gamma*np.max(q_[i]))
        return target


class PTQAgent(QAgent):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, det_action, need_greedy, gamma, net, tnet):
        super(PTQAgent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace,
                                    gd=gd, 
                                    det_action=det_action, 
                                    need_greedy=need_greedy,
                                    gamma=gamma,
                                    net=net)
        self.tnet = tnet
        self.target_replace_op = []

    def init_session(self):
        self.tnet.init()
        super().init_session()
        self.copyq()

    def load(self):
        super().load()
        self.tnet.load()
        self.copyq()
    
    def get_pq(self, obses):
        if self.tnet.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = {self.tnet.obses: np.array(obses)}
        p, q = self.sess.run([self.tnet.out_prob, self.tnet.n_out], feed_dict=feed_dict)
        return p, q

    def copyq(self):
        t_params = self.tnet.get_trainable()
        e_params = self.net.get_trainable()
        self.target_replace_op = [tf.assign(t, e) for t, e in zip(t_params, e_params)]
 
    def train(self, obses, actions, obses_, rewards, dones):
        super().train(obses, actions, obses_, rewards, dones)
        self.sess.run(self.target_replace_op) 
