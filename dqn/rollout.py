from common.rollout import RollOutBase
from common.result_base import ResultSummary

class QRollOut(RollOutBase):
    def __init__(self, maxstep=500, clear=False, render=False, sleep =0, aug_rew=None, agent=None, env=None, summary=None):
        super(QRollOut, self).__init__(maxstep, clear, render, sleep, aug_rew, agent, env, summary)

    def sample(self, obs):
        act = self.agent.get_acts([obs])
        obs_, rew, done, info = self.env.step(act)
        if self.aug_rew:
            rew = self.aug_rew.get_reward(done=done, 
                                          rew=rew, 
                                          obs=obs, 
                                          obs_=obs_)
        return obs, act, obs_, rew, done

