import time
from common.rollout import RollOutBase, ResultSummary
from common.rollout import RollOutBase
from common.result_base import ResultSummary

class PGRollOut(RollOutBase):
    def __init__(self, maxstep=500, clear=False, render=False, sleep =0, aug_rew=None, agent=None, env=None):
        super(PGRollOut, self).__init__(maxstep, clear, render, sleep, aug_rew, agent, env)
        self.summary = ResultSummary()

    def sample(self):
        obs, rew, done, info = self.env.reset(), 0 , False, {}
        obses, acts, rews, dones = [], [], [], []
        while not done:
            act = self.agent.get_acts([obs])
            obs_, rew, done, info = self.env.step(act)
            if self.aug_rew:
                rew = self.aug_rew.get_reward(done=done, 
                                              rew=rew, 
                                              obs=obs, 
                                              obs_=obs_,
                                              rew_length=len(rews))
            acts.append(act)
            obses.append(obs)
            rews.append(rew)
            dones.append(done)
            obs = obs_
        return obses, acts, rews, dones


