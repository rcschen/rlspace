from common.agent_base import AgentBase
from .net import PG

class PGAgent(AgentBase):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, optimizer='ada'):
        super(PGAgent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy)

        self.net = PG(env=env, namespace=namespace, learnrate=learnrate, activation=activation, optimizer=optimizer)


