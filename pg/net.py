import tensorflow as tf
from common.net_base import NetBase
from common.inputs import get_ph_by_env


class PG(NetBase):
    def __init__(self, env, namespace, learnrate, activation, optimizer):
        super(PG, self).__init__(env, namespace, learnrate, activation, optimizer=optimizer)
 
    def load(self):
        super().load()
        self.obses = tf.get_default_graph().get_tensor_by_name('{}/obses:0'.format(self.namespace))
        self.acts = tf.get_default_graph().get_tensor_by_name('{}/acts:0'.format(self.namespace))
        self.rews = tf.get_default_graph().get_tensor_by_name('{}/rews:0'.format(self.namespace))
        self.n_out = tf.get_default_graph().get_tensor_by_name('{}/n_out:0'.format(self.namespace))
        self.out_prob = tf.get_default_graph().get_tensor_by_name('{}/out_prob:0'.format(self.namespace))
        self.loss = tf.get_default_graph().get_tensor_by_name('{}/loss:0'.format(self.namespace))
        self.optimize = tf.get_default_graph().get_operation_by_name('{}/optimize'.format(self.namespace))
 
    def set_placeholder(self):
        with tf.variable_scope(self.namespace): 
            self.obses, self.obses_pro = get_ph_by_env(self.env.observation_space, name='obses')
            self.acts = tf.placeholder(tf.int32, name='acts')
            self.rews = tf.placeholder(tf.float32, name='rews')
       
    def set_optimize(self):
        self.optimize_section_CEL()

    def optimize_section(self):
        with tf.variable_scope('{}/'.format(self.namespace)):
            self.out = - tf.reduce_sum(tf.log(self.out_prob)*tf.one_hot(self.acts, self.env.action_space.n), axis=1)
            tf.summary.scalar('rews', self.rews)
            self.loss = tf.reduce_mean(self.out * self.rews, name='loss')
            tf.summary.scalar('loss', self.loss)
            optimizer = self.optimizer(self.learnrate)
            self.optimize = optimizer.minimize(self.loss, name='optimize')
    
    def optimize_section_CEL(self):
        with tf.variable_scope('{}/'.format(self.namespace)):
            # to maximize total reward (log_p * R) is to minimize -(log_p * R), and the tf only have minimize(loss)
            print(tf.cast(tf.one_hot(self.acts, self.env.action_space.n),tf.int64))
            self.out = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.n_out, 
                                                                  labels=self.acts)   # this is negative log of chosen action
            # or in this way:
            # neg_log_prob = tf.reduce_sum(-tf.log(self.all_act_prob)*tf.one_hot(self.tf_acts, self.n_actions), axis=1)
            self.loss = tf.reduce_mean(self.out * self.rews, name='loss')  
            tf.summary.scalar('loss', self.loss)
            optimizer = self.optimizer(self.learnrate)
            self.optimize = optimizer.minimize(self.loss, name='optimize')

