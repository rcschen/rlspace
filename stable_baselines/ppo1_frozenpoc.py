import gym, os, time

from stable_baselines.common.policies import MlpPolicy
from stable_baselines.common import make_vec_env
from stable_baselines import  PPO1
from com_frozenpoc import ResultSummary

logs_path = os.path.join('__d_logs',__file__.split('.')[0], str(int(time.time())))

model_file = __file__.split('.')[0]
# Parallel environments
env = make_vec_env('FrozenLake-v0', n_envs=1)

model = PPO1(MlpPolicy, env, 
            verbose=2,
            tensorboard_log=logs_path)
model.learn(total_timesteps=1000000)
model.save(model_file)

del model # remove to demonstrate saving and loading

model = PPO1.load(model_file)
r = ResultSummary()
obs = env.reset()
for i in range(1000):
    done = False
    while not done:
        action, _states = model.predict(obs)
        obs, rewards, dones, info = env.step(action)
        done = dones[0]
        r.set_summary(dones[0], rewards[0])
        #env.render()
print(r.cal_result()) 

