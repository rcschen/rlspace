import gym, os, time

from stable_baselines.common.atari_wrappers import make_atari
from stable_baselines.deepq.policies import MlpPolicy, CnnPolicy
from stable_baselines import DQN


logs_path = os.path.join('__d_logs',__file__.split('.')[0], str(int(time.time())))

model_file = __file__.split('.')[0]
# Parallel environments
env = make_atari('AirRaidNoFrameskip-v0')

model = DQN(CnnPolicy, env, verbose=2, tensorboard_log=logs_path)
print('start to learn')
model.learn(total_timesteps=2500)
print('learn complete')
model.save(model_file)

del model # remove to demonstrate saving and loading

model = DQN.load(model_file)

obs = env.reset()
while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()



