class ResultSummary(object):
    def __init__(self):
        self.hist = None
        self.reset_hist()

    def cal_result(self):
        print(self.hist)
        return float(self.hist['G'] / (self.hist['G'] + self.hist['H']))

    def reset_hist(self):
        self.hist = {'G':0, 'H':0}
 
    def set_summary(self, done, rew):
        if done:
            if rew <= 0:
                self.hist['H'] += 1
#                print('T____________T')
            else:
                self.hist['G'] += 1
#                print('>____________^')
            
