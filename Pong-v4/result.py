from common.result_base import ResultSummary


class Result(ResultSummary):
    def __init__(self):
        super(Result, self).__init__()
        self.hist = []
        self.count = 0
        self.total_reward_round = 0

    def cal_result(self):
        return self.count

    def reset_hist(self):
        self.hist = []
 
    def set_summary_each_step(self, **kwargs):
        self.count+=1
        self.total_reward_round += kwargs['rew']

    def set_summary(self, done, rew):
        self.hist.append('{}/{}'.format(self.total_reward_round,self.count))
        self.count=0
        self.total_reward_round = 0
        
