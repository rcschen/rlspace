import __init__
from gym import wrappers 
from common.env_base import BaseEnv
from common import wrappers as wp
from common import atari_wrappers as awp
import gym, cv2
import numpy as np

def make_env(env_id):
    env = gym.make(env_id)
    env = wp.InitEnvDType(env)
    print('org dtype:{}'.format(env.observation_space.dtype))
    env = wp.MaxAndSkipEnv(env)
    print('wp.MaxAndSkipEnv dtype:{}'.format(env.observation_space.dtype))

    env = wp.FireResetEnv(env)
    print('wp.FireResetEnv dtype:{}'.format(env.observation_space.dtype))

    env = wp.ProcessFrame84(env)
#    env = wp.ImageToPyTorch(env)
#    env = wp.BufferWrapper(env, 4)
#    env = wp.WarpFrame(env)
    print('wp.WarpFrame dtype:{}'.format(env.observation_space.dtype))
    env = wp.FrameStack(env, 4)
    print('wp.FrameStack dtype:{}'.format(env.observation_space.dtype))
    env = wp.ScaledFloatFrame(env)
    print('wp.ScaledFloatFrame dtype:{}'.format(env.observation_space.dtype))
    return env 
 
class Env(BaseEnv):
    def __init__(self, round_mgt=None):
        super(Env, self).__init__()
        self.env = make_env('PongNoFrameskip-v4')
        self.round_mgt = round_mgt

    def get_env(self):
        self.env.observation_space.n=self.env.observation_space.shape[0]
        self.env = wrappers.Monitor(self.env, 
                                    '__d_record', 
                                    force=True,
                                    video_callable=lambda episode: self.round_mgt.get_notification('video')) 
        return self.env

    def set_probability(self):
        pass

if __name__ == '__main__':
    env = Env().get_env()
    obs = np.array(env.reset())
    for i in range(500):
        obs, _, _, _ = env.step(1)
    obs = np.array(obs)    
    print(env.observation_space.dtype)
    cv2.imwrite('0.jpg', obs[...,0]) 
    cv2.imwrite('1.jpg', obs[...,1]) 
    cv2.imwrite('2.jpg', obs[...,2]) 
    cv2.imwrite('3.jpg', obs[...,3]) 

