import __init__
from common.env_base import BaseEnv
from gym import wrappers
from gym import register


class Env(BaseEnv):
    def __init__(self, slip=True, one_det=False):
        register(id='FrozenLakeNotSlippery-v0',
                 entry_point='gym.envs.toy_text:FrozenLakeEnv',
                 kwargs={'map_name' : '4x4', 'is_slippery': False},)
        self.env = None
        if slip:
            env_name  = 'FrozenLake-v0'
            super(Env, self).__init__(env_name)
            if one_det:
                fe = self.set_probability()
        else:
            env_name = 'FrozenLakeNotSlippery-v0'
            super(Env, self).__init__(env_name)
 
    def get_env(self):
        return self.env

    def set_probability(self):
        self.env.P[0][1] = [(1.0, 4, 0.0, False)]
#        print(self.env.P[0])
        self.env.P[4][1] = [(1.0, 8, 0.0, False)]
#        print(self.env.P[4])
        self.env.P[8][2] = [(1.0, 9, 0.0, False)]
#        print(self.env.P[8])
        self.env.P[9][1] = [(1.0, 13, 0.0, False)]
#        print(self.env.P[9])
        self.env.P[13][2] = [(1.0, 14, 0.0, False)]
#        print(self.env.P[13])
        self.env.P[14][2] = [(1.0, 15, 1.0, True)]
#        print(self.env.P[15])




if __name__ == '__main__':
    env = Env().get_env()
    print(env.observation_space.n)
