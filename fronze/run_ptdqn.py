import __init__, os, time
from env import Env
from dqn.argument import Argument
from dqn.dqn import PTExecutor
from common.utils import check_path

logs_path = os.path.join('__d_logs',__file__.split('.')[0], str(int(time.time())))
PATH = check_path( os.path.join('__d_model',__file__.split('.')[0]))
PATH = os.path.join( PATH, 'model')

if __name__ == '__main__':
    arg_obj = Argument()
    arg_obj.add_specific("--slip", type=bool, help='slip', default=True)
    arg_obj.add_specific("--one_det", type=bool, help='one det', default=False)
    args = arg_obj.get_args()
    print(args)
    env = Env(args.slip, args.one_det).get_env()
    exe = PTExecutor(env, args, PATH, logs_path)
    getattr(exe, args.type)()
'''
python run_dqn.py -t train -c f --greedy_denom 10000 --learn_rate 0.001 --need_greedy true --play_load true --play_render true --play_round 100 --play_clear "" --play_sleep 0 --slip true --one_det "" --gamma 0.96 --train_epoch 10000 --reward_type 0 --det_action true --activation no_act --rew_scale 100 --data_len 1000 --batch_size 32
'''
