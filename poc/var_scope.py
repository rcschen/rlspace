import tensorflow as tf
import time, os

logs_path = os.path.join('__d_logs',__file__.split('.')[0], str(int(time.time())))

def outer_scope_getter(scope, new_scope=""):
    """
    remove a scope layer for the getter

    :param scope: (str) the layer to remove
    :param new_scope: (str) optional replacement name
    :return: (function (function, str, ``*args``, ``**kwargs``): Tensorflow Tensor)
    """
    def _getter(getter, name, *args, **kwargs):
        name = name.replace(scope + "/", new_scope, 1)
        print('==name==> {}'.format(name))
        print('==args==> {}'.format(args))
        print('==kwargs==> {}'.format(kwargs))

        val = getter(name, *args, **kwargs)
        return val
    return _getter

def get_input(shape=[]):
    with tf.variable_scope('input'):
        return tf.placeholder(tf.float32, shape=shape)
def get_var(scope, name ,shape=[3,2]):
    with tf.variable_scope(scope):
        return tf.get_variable(name, shape=shape)

def net(fin, reuse):
    with tf.variable_scope('net', reuse=reuse):
        w = get_var('weight', 'weight', [3,2])
        b = get_var('bias', 'bias', [2])       
        return tf.nn.bias_add(tf.matmul(fin, w),b)

def output(fin):
    with tf.variable_scope('output'):
        return tf.nn.softmax(fin) 

with tf.Graph().as_default():
    fin =  get_input([10,3])
    model_output = net(fin, False)
    out = output(model_output)
    with tf.variable_scope('train', reuse=True, custom_getter=outer_scope_getter("train")):
        train_in = get_input([10,3])
        train_out = output(net(train_in, True))
      
    writer = tf.summary.FileWriter(logs_path, graph=tf.get_default_graph())

print(model_output)
print(train_out)

