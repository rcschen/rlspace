import gym


class MyWrapperEnv(gym.Wrapper):
    def __init__(self, env=None):
        super(MyWrapperEnv, self).__init__(env)
        print('init MyWrapperEnv')
    def step(self, action):
        print('MyWrapperEnv step', action)
        res= self.env.step(action)
        return res
  
    def reset(self):
        print('MyWrapperEnv reset')

        return self.env.reset()
       

class MyWrapper2Env(gym.Wrapper):
    def __init__(self, env=None):
        super(MyWrapper2Env, self).__init__(env)
        print('init MyWrapper2Env')

    def step(self, action):
        print('MyWrappert2Env step', action)
        res= self.env.step(action)
        return res

    def reset(self):
        print('MyWrapper2Env reset')
        return self.env.reset()

class MyObs2Env(gym.ObservationWrapper):
    def observation(self, obs):
        print('MyObs2Env obs')
        return obs

class MyObsEnv(gym.ObservationWrapper):
    def __init__(self, env=None):
        super(MyObsEnv, self).__init__(env)
        print('init MyObsEnv')

    def observation(self, obs):
        print('MyObsEnv obs')
        return obs

    def step(self, action):
        print('MyObsEnv step', action)
        res= self.env.step(action)
        return res

    def reset(self):
        print('MyObsEnv reset')
        return self.observation(self.env.reset())

class MyActEnv(gym.ActionWrapper):
    def __init__(self, env=None):
        super(MyActEnv, self).__init__(env)
        print('init MyActEnv')

    def observation(self, obs):
        print('MyActEnv obs')
        return obs

    def step(self, action):
        print('MyActEnv step', action)
        res= self.env.step(action)
        return res

    def reset(self):
        print('MyActEnv reset')
        return self.env.reset()
    def action(self, action):
        print('MyActEnv action')
        return action

class MyAct2Env(gym.ActionWrapper):
    def reset(self):
        print('MyAct2Env reset')
        return self.env.reset()

    def action(self, action):
        print('MyAct2Env action')
        return action

def make_env(ename):
    env = gym.make(ename)
#    env = MyWrapperEnv(env)
#    env = MyWrapper2Env(env)
#    env = MyObsEnv(env)
#    env = MyObs2Env(env)

    env = MyActEnv(env)
    env = MyAct2Env(env)

    return env 

if __name__ == "__main__":
    ENV_NAME = "PongNoFrameskip-v4"
    env = make_env(ENV_NAME)
    env.reset()
    env.step(1)

