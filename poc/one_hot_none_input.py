import tensorflow as tf
labels=tf.placeholder(shape=(None,), dtype=tf.int32)
size = labels.get_shape()[0]
highest_label = tf.reduce_max(labels)
labels_t = tf.reshape(labels, [-1, 1])
indices = tf.reshape(tf.range(size), [-1, 1])
idx_with_labels = tf.concat([indices, labels_t], 1)
labels_one_hot = tf.sparse_to_dense(idx_with_labels, [size, highest_label + 1], 1.0)




#labels = tf.constant([5,3,2,4,1])


