'''
ref: https://zhuanlan.zhihu.com/p/47124891
'''
from functools import partial
def func(*, a):
    print(a)

funcp=partial(func, a=10)
funcp()

def func1(a):
    print(a)
funcc=partial(func, a=10)
funcc()
