from common.result_base import ResultSummary


class Result(ResultSummary):
    def __init__(self):
        super(Result, self).__init__()
        self.hist = []
        self.count = 0

    def cal_result(self):
        return self.count

    def reset_hist(self):
        self.hist = []
 
    def set_summary_each_step(self, *args, **kwargs):
        self.count+=1

    def set_summary(self, done, rew):
        self.hist.append(self.count)
        self.count=0
        
