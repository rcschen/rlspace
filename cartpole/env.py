import __init__
from common.env_base import BaseEnv


class Env(BaseEnv):
    def __init__(self):
        super(Env, self).__init__('CartPole-v1')
 
    def get_env(self):
        self.env.observation_space.n=self.env.observation_space.shape[0]
        return self.env

    def set_probability(self):
        pass

if __name__ == '__main__':
    env = Env().get_env()
    print(dir(env))
    print(env.observation_space.n)
