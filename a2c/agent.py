import tensorflow as tf
 
from common.agent_base import AgentBase
from .net import VNet, A2CNet, A2CNetCEL
from pg.net import PG

class A2CAgent(AgentBase):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, gamma, optimizer='ada'):
        super(A2CAgent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy)
        self.gamma = gamma
        self.critic_ns = '{}_critic'.format(namespace)
        self.actor_ns = '{}_actor'.format(namespace)
        self.critic = VNet(env=env, 
                           namespace=self.critic_ns, 
                           learnrate=learnrate, 
                           activation=activation,
                           gamma=gamma,
                           optimizer=optimizer)
        self.actor = PG(env=env, 
                        namespace=self.actor_ns, 
                        learnrate=learnrate, 
                        activation=activation,
                        optimizer=optimizer)

    def init_summary(self):
        self.sm.init_writer()
        self.sm_actor = self.sm.get_summary_merge(self.actor_ns)
        self.sm_critic = self.sm.get_summary_merge(self.critic_ns)
        
    def init_session(self):
        self.actor.init()
        self.critic.init()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)
        if self.debug:
            self.sess = tf_debug.TensorBoardDebugWrapperSession(self.sess, "localhost:8887")
        self.init_summary()

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.actor.load()
        self.critic.load()

    def get_critic_target(self, obses, rews, dones):
        target = []
        v = self.get_critic_v(obses)
        for i, d in enumerate(dones):
#            if d:
#                target.append(rews[i])           
#            else:
                target.append(rews[i]+self.gamma*v[i,0])
        return target

class ShareA2CAgent(AgentBase):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, gamma, optimizer='ada'):
        super(ShareA2CAgent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy)
        self.gamma = gamma
        self.critic_ns = '{}_critic'.format(namespace)
        self.actor_ns = '{}_actor'.format(namespace)
        self.net = A2CNet(env=env, 
                          namespace=namespace, 
                          learnrate=learnrate, 
                          activation=activation, 
                          critic_ns = self.critic_ns, 
                          actor_ns = self.actor_ns,
                          optimizer=optimizer)

    def init_summary(self):
        self.sm.init_writer()
        self.sm_actor = self.sm.get_summary_merge(self.actor_ns)
        self.sm_critic = self.sm.get_summary_merge(self.critic_ns)
        print('+++++++++++++++++++++++++++++++')
        print(self.sm_actor) 
        print('+++++++++++++++++++++++++++++++------------------') 
        print(self.sm_critic) 
        print('+++++++++++++++++++++++++++++++') 

    def init_session(self):
        self.net.init()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)
        if self.debug:
            self.sess = tf_debug.TensorBoardDebugWrapperSession(self.sess, "localhost:8887")
        self.init_summary()

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.net.load()

    def get_critic_target(self, obses, rews, dones):
        target = []
        v = self.get_critic_v(obses)
        for i, d in enumerate(dones):
            if d:
                target.append(rews[i])           
            else:
                target.append(rews[i]+self.gamma*v[i,0])
        return target

class ShareA2CAgentCEL(ShareA2CAgent):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, gamma, optimizer='ada'):
        super(ShareA2CAgentCEL, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy, 
                                    gamma=gamma)
        self.net = A2CNetCEL(env=env, 
                          namespace=namespace, 
                          learnrate=learnrate, 
                          activation=activation, 
                          critic_ns = self.critic_ns, 
                          actor_ns = self.actor_ns,
                          optimizer=optimizer)

